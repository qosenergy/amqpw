---
stages:
  - test
  - deploy

default:
  image: golangci/golangci-lint:v1.58.2

check-code-quality:
  stage: test
  script:
    - echo "Running golangci-lint"
    - golangci-lint --version
    - golangci-lint run --exclude-use-default=false

check-modules-update:
  stage: test
  allow_failure: true
  script:
    - echo "Checking direct for go module updates ..."
    - OUT_OF_DATE=`go list -f '{{if and .Update (not .Indirect)}} {{.String}} {{end}}' -m -u all`
    - >
      if [ "$OUT_OF_DATE" != "" ]; then
        echo "FAIL - Following modules should be updated with go get -u:"
        printf '%s\n' "$OUT_OF_DATE"
        exit 1
      fi
    - echo "OK - Direct go modules are up to date"

tests:
  stage: test
  script:
    - go install github.com/axw/gocov/gocov@latest
    - go install github.com/AlekSi/gocov-xml@latest
    - echo "== TEST =="
    - echo "Running tests"
    - GOTESTFLAGS=""
    - >
      if [ "${FAILFAST:-0}" == "1" ]; then
        GOTESTFLAGS="-failfast"
      fi
    - TAGS=""
    - >
      if [ -n "${GOTEST_TAGS:-}" ]; then
        TAGS="-tags=$GOTEST_TAGS"
      fi
    - go test ${TAGS:+"$TAGS"} ${GOTESTFLAGS:+"$GOTESTFLAGS"} -p "${GOTESTS_PARALLEL:-1}" -v -race -timeout 30m -coverprofile coverage.tmp ./...
    - cat coverage.tmp | grep -v "mock_" > coverage.out
    - echo "Generating coverage report"
    - mkdir -p "coverage/$CI_COMMIT_REF_NAME"
    - go tool cover -html=coverage.out -o="coverage/$CI_COMMIT_REF_NAME/index.html"
    # Format coverage as GitlabCI readable XML
    - gocov convert coverage.out > coverage.json
    - gocov-xml < coverage.json > coverage.xml
    - go tool cover -func coverage.out # Last line contains the total coverage
  coverage: '/\(statements\)\s*\d+.\d+%/'
  artifacts:
    paths:
      - coverage
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

pages:
  stage: deploy
  dependencies:
    - tests
  only:
    - master
  script:
    - mkdir public
    - mv coverage public/coverage
    - echo "Pages accessible through ${CI_PAGES_URL}/${PAGES_PREFIX}"
  artifacts:
    paths:
      - public
