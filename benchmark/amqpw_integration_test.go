//go:build integration

package benchmark

import (
	"context"
	"os"
	"sync"
	"testing"

	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/qosenergy/amqpw"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

func BenchmarkProduce(b *testing.B) {
	uri, ok := os.LookupEnv(test.AmqpURIKey)
	if !ok || uri == "" {
		b.Errorf("%q env var missing", test.AmqpURIKey)
	}

	exchange := "bench_exchange"
	queue := "bench_queue"

	// clear the amqp topology before tests
	_ = test.ExchangeDelete(uri, exchange)
	_ = test.QueueDelete(uri, queue)

	if err := test.ExchangeDeclare(uri, exchange, amqp091.ExchangeFanout); err != nil {
		b.Errorf("exchange create: %s", err.Error())
	}
	if err := test.QueueDeclare(uri, queue); err != nil {
		b.Errorf("queue create: %s", err.Error())
	}
	if err := test.BindDeclare(uri, exchange, queue, ""); err != nil {
		b.Errorf("bind create: %s", err.Error())
	}

	b.Run("1 channel publish", func(b *testing.B) {
		b.StopTimer()
		benchBroker := amqpw.NewBroker(context.Background(), amqpw.Conf{
			URI:      uri,
			ConnName: "benchmark",
			Producer: &amqpw.ProducerConf{},
		})
		// ensure everything is done starting
		b.StartTimer()
		b.SetParallelism(100)
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				pubErr := benchBroker.Publish(
					exchange, "", amqp091.Publishing{},
					nil,
				)
				if pubErr != nil {
					b.Logf("publish err: %s", pubErr)
				}
			}
		})
		if err := benchBroker.Close(); err != nil {
			b.Errorf("closing broker: %s", err.Error())
		}
	})
	b.Run("10 channel publish", func(b *testing.B) {
		b.StopTimer()
		benchBroker := amqpw.NewBroker(context.Background(), amqpw.Conf{
			URI:      uri,
			ConnName: "benchmark",
			Producer: &amqpw.ProducerConf{
				ChannelPoolSize: 10,
			},
		})
		// ensure everything is done starting
		b.StartTimer()
		b.SetParallelism(100)
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				pubErr := benchBroker.Publish(
					exchange, "", amqp091.Publishing{},
					nil,
				)
				if pubErr != nil {
					b.Logf("publish err: %s", pubErr)
				}
			}
		})
		if err := benchBroker.Close(); err != nil {
			b.Errorf("closing broker: %s", err.Error())
		}
	})
	b.Run("consume", func(b *testing.B) {
		b.StopTimer()
		benchBroker := amqpw.NewBroker(context.Background(), amqpw.Conf{
			URI:      uri,
			ConnName: "benchmark",
			Producer: &amqpw.ProducerConf{},
			Consumer: &amqpw.ConsumerConf{
				QOS: amqpw.NewQOS(1000),
			},
		})
		done := make(chan struct{})
		var wg sync.WaitGroup
		for i := 0; i < 100; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				for {
					select {
					case <-done:
						return
					default:
					}
					_ = benchBroker.Publish(
						exchange, "",
						amqp091.Publishing{Body: []byte("test")},
						nil,
					)
				}
			}()
		}
		consumeChan, consumeErr := benchBroker.Consume(
			queue, "bench consume",
			nil,
		)
		if consumeErr != nil {
			b.Errorf("consume error: %s", consumeErr.Error())
		}

		b.StartTimer()
		b.SetParallelism(100)
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				msg := <-consumeChan
				ackErr := benchBroker.Ack(msg, false)
				if ackErr != nil {
					b.Logf("ack err: %s", ackErr)
				}
			}
		})
		close(done)
		wg.Wait()
		if err := benchBroker.Close(); err != nil {
			b.Errorf("closing broker: %s", err.Error())
		}
	})
}
