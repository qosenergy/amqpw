//go:build integration

package amqpw

// revive:disable:dot-imports

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"testing"
	"time"

	"github.com/rabbitmq/amqp091-go"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

// revive:enable:dot-imports

func TestBroker(t *testing.T) {
	Convey("broker integration test", t, func() {
		testTimer := time.NewTimer(5 * time.Second)

		uri, ok := os.LookupEnv(test.AmqpURIKey)
		SoMsg(fmt.Sprintf("%q env var missing", test.AmqpURIKey), ok, ShouldBeTrue)

		cancellableCtx, cancel := context.WithCancel(context.Background())
		broker := NewBroker(cancellableCtx, Conf{
			URI:      uri,
			ConnName: "broker test",
			Consumer: &ConsumerConf{
				QOS: NewQOS(10),
			},
			Producer: &ProducerConf{},
		})

		exchange := "broker_exchange"
		queue := "broker_queue"

		// clear the amqp topology before tests
		_ = test.ExchangeDelete(uri, exchange)
		_ = test.QueueDelete(uri, queue)

		ok, err := broker.CheckExchange(exchange)
		SoMsg("checking exchange returns no error", err, ShouldBeNil)
		SoMsg("... but the exchange does not exist yet", ok, ShouldBeFalse)
		ok, _, err = broker.CheckQueue(queue)
		SoMsg("checking queue returns no error", err, ShouldBeNil)
		SoMsg("... but the queue does not exist yet", ok, ShouldBeFalse)

		SoMsg("exchange create", test.ExchangeDeclare(uri, exchange, amqp091.ExchangeFanout), ShouldBeNil)
		SoMsg("queue create", test.QueueDeclare(uri, queue), ShouldBeNil)
		SoMsg("bind create", test.BindDeclare(uri, exchange, queue, ""), ShouldBeNil)

		ok, err = broker.CheckExchange(exchange)
		SoMsg("checking exchange returns no error", err, ShouldBeNil)
		SoMsg("... and the exchange exists", ok, ShouldBeTrue)
		ok, _, err = broker.CheckQueue(queue)
		SoMsg("checking queue returns no error", err, ShouldBeNil)
		SoMsg("... and the queue exists", ok, ShouldBeTrue)

		//TODO ErrChan

		SoMsg(
			"publishing a messages returns no error",
			broker.Publish(exchange, "", amqp091.Publishing{
				Body: []byte("test"),
			}, nil),
			ShouldBeNil,
		)

		consumeChan, consumeErr := broker.Consume(queue, "broker test", nil)
		SoMsg("consuming started with no error", consumeErr, ShouldBeNil)

		var msg amqp091.Delivery
		select {
		case <-testTimer.C:
		case msg = <-consumeChan:
			ok = true
		}
		SoMsg("... and returns an AMQP message", ok, ShouldBeTrue)
		SoMsg("... and the content matches the previously published msg", string(msg.Body), ShouldEqual, "test")
		SoMsg("... and that message can be Ack() with no error", msg.Ack(false), ShouldBeNil)

		cancel()

		var open bool
		select {
		case <-testTimer.C:
		case _, open = <-consumeChan:
			ok = true
		}
		SoMsg(" after context cancel, we can read from consume channel", ok, ShouldBeTrue)
		SoMsg("... because it was closed", open, ShouldBeFalse)
		SoMsg(
			"... but we can still publish",
			broker.Publish(exchange, "", amqp091.Publishing{
				Body: []byte("test"),
			}, nil),
			ShouldBeNil,
		)

		SoMsg("closing broker returns no error", broker.Close(), ShouldBeNil)
		SoMsg(
			"... and publish then returns EOF",
			errors.Is(
				broker.Publish(
					exchange, "", amqp091.Publishing{
						Body: []byte("test"),
					}, nil,
				),
				io.EOF,
			),
			ShouldBeTrue,
		)
	})
}
