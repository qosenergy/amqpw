
# Links

[![pipeline status](https://gitlab.com/qosenergy/amqpw/badges/pipeline.svg)](https://gitlab.com/qosenergy/amqpw/commits)
[![coverage report](https://gitlab.com/qosenergy/amqpw/badges/coverage.svg)](https://gitlab.com/qosenergy/amqpw/commits)

# Use

This library exposes a broker to ease interaction with RabbitMQ through official lib amqp091.

It allows Consuming and Producing from existing queues and exchanges without having to handle disconnections
and handling of the various layers needed to interact with RabbitMQ (connection & channels).
It also exposes retry mecanisms and simple checks on topology.

## 1-Broker

The lib "gitlab.com/qosenergy/amqpw/broker" duplicates "gitlab.com/qosenergy/amqpw"
but won't be updated anymore, so the former has been deprecated. Please use the latter.

### Init Broker

Create a broker with RabbitMQ URI of format "amqp://\$(AMQP\_LOGIN):\$(AMQP\_PASSWORD)@\$(AMQP\_ADDRESS)/\$(RMQ\_VHOST)"  
To get the URI from env var using [github.com/Netflix/go-env](github.com/Netflix/go-env), prefer using the exposed **Env** struct  
to normalize env var name (**RMQ_SERVER**)

[Consumer](#2-consume) conf needs to be filled if broker will be used to consume

[Producer](#3-producer) conf needs to be filled if broker will be used to publish

It is advised to listen and log **ErrChan()** which will send any internal error that occured.  
This is for logging exclusively. Specific behaviour after an error is handled internally.

### Init Broker example

`import "gitlab.com/qosenergy/amqpw"`

FYI: logger used in examples is zerolog

```golang
amqpwBroker := amqpw.NewBroker(ctx,amqpw.Conf{
  URI: envVarURI,
  ConnName: podName,
  Consumer: &ConsumerConf{
    ChannelPoolSize: 1,
    QOS: amqpw.NewQOS(100),
  },
  Producer: &ProducerConf{
    ChannelPoolSize: 1,
  },
})

go func(errChan <-chan error){
  for err := range errChan {
    log.Warn().Err(err).Msg("amqpw internal error")
  }
}
```

### broker Closing funcs

**Close()** stops all "in progress" work in the producer, then closes connection (which closes both consumer & producer).  

**Wait()** blocks until all ongoing publishing in the producer is done.  
Make sure you stopped publishing routines before calling **Wait()**

For a clean broker close:

1. first **Cancel** or **ctx.Close** the consumer
2. then finish handling all messages & **Ack/Nack** them
3. then **Close** the broker

## 2-Consume

### ConsumerConf

Needs not nil Conf to consume:

- **ChannelPoolSize** should be a divider of the number of consumers you intend to have.  
It can be 1 if you expect little traffic,  
it should not be greater than 10 to comply with Rabbit's limitations.
- **QOS** should have a non 0 prefetch count  
(advised value is twice what you compute in parallel. performances test can help fine-tune).

### Consume

To create a new consumer, just call **Consume()** with queue name & unique tag.
You can consume the output chan to get messages.

If you call **Consume()** after closing the **context.Context** of the broker, it will return **io.EOF**
(to check with *Errors.Is*)

If you call Consume with invalid conf (bad URI, non existant queue, etc.),
it will immediately return a [non-retriable error](#4-retriable-errors).

Once you are done with a message consumed, do not forget to **Ack()** it with **msg.Ack** or **broker.Ack**
Since the consumer reconnects automatically, **msg.Ack** can return an **amqp091.Closed** (with **errors.Is**),
even if the consumer still runs normally after automatically reconnecting.
This means the connection was lost and the message pushed back to RMQ: you can continue running in this case.
Otherwise, it is advised to either retry **Ack()** or crash to avoid locked messages in RMQ.

To avoid repeated testing of **amqp091.ErrClosed**, **Ack()** and **Nack()** function where added to the broker.
They just call the associated method of the function but replace **amqp091.ErrClosed** with nil to ignore it.
If you need a specific behaviour, prefer the use of **msg.Ack()** with **amqpwerror.IsErrClosed(err)**
(for instance if you need to rollback operations on Ack/Nack failure).

### Cancel or ctx.Done

To properly end consuming, you can:

- call **Cancel()** with the tag of a started consumer; if it returns without error, the chan of the consumer will close.  
If a [retriable error](#4-retriable-errors) is returned, it means cancellation failed and should be retried.  
If a [non-retriable error](#4-retriable-errors) is returned, it means you tried to cancel an already cancelled consumer.
- close the **context.Context** given to the broker, it will close output chan of all consumer  
(only use this to end your program as proper Cancel on RabbitMQ side is not guaranteed).

Connection to RabbitMQ remains open in both cases (you can stil **Ack()** messages)

### Unwanted Close

If the output chan closes without you calling **Cancel** or closing the context,
then a non-retriable internal error occured. It is advised to close and restart your program.

### Consume example

See [broker init](#init-broker-example) to initialize **amqpwbroker**

```golang
consumeChan, consumeErr := amqpwbroker.Consume(queueName, uniqueConsumerTag, nil)
if consumeErr != nil {
  if amqpwerror.IsErrRetriable(consumeErr) {
    // retry ?
    log.Warn().Err(consumeErr).
      Str("queue",queueName).Str("tag",uniqueConsumeTag).
      Msg("consume failed with retriable error")
  } else {
    // bad conf: crash
    log.Error().Err(consumeErr).
      Str("queue",queueName).Str("tag",uniqueConsumeTag).
      Msg("consume error")
  }
}

for msg := range consumeChan {
  // Handle msg
  ackErr := amqpwbroker.Ack(msg,false)
  log.Warn().Err(ackErr).Msg("failed to ack amqp Delivery")
}

// identify if chan close is due to program action or accidental & exit program if accidental
```

## 3-Producer

### ProducerConf

Needs not nil Conf to produce:

- **ChannelPoolSize** can be 1 if you do not expect a big traffic.  
Otherwise, a higher value can be used.
It should not be greater than 10 to comply with Rabbit's limitations.

### Produce funcs

4 funcs exist to publish:

- Publish
- PublishWithRetry
- PublishASync
- PublishASyncWithRetry

**ASync** means the function will return before the message was effectively published
(callback func in parameter will be called once we receive publish confirm).  
An inner security exists to avoid memory use explosion if you suddenly publish a lot
(call will block until some messages are done publishing).  
No ASync means the call is fully synchronous and only returns after confirmation or failure.

**WithRetry** means the backoff policy in parameter will be used to retry what can be internally.  
**statusChan** parameter can be used to get error messages for each retry (for logging purposes);
it is unused if nil BUT can block if not nil & not consumed.

If the producer or the broker's context is closed, current and new publish will return an error
that wraps **io.EOF**

### Produce example

See [broker init](#init-broker-example) to initialize **amqpwbroker**

```golang
msg := amqp091.Publishing{
  Body: []byte("my msg body")
}
callback func(published bool,err error) {
  if err != nil {
    // log/handle error
  }
  if !published {
    // react to publish failure
  } else {
    // react to publish success
  }
}
amqpwbroker.PublishASyncWithRetry(
  "exchangeName",
  "routingKey",
  msg,
  callback,
  backoff.NewExponentialBackoff(),
  nil,
)
```

## 4-Retriable Errors

`import amqpwerror "gitlab.com/qosenergy/amqpw/error"`

In case you want to implement your own retry around **Consume()** or **Publish\*()**:  
You can use the lib amqpwerror from this repository to detect retriable errors.

The main use case is **Consume()** instantly returning a retriable error. Then you can retry on your side.

In case of **Consume()** having an error while running, an inner retry policy already exists.  
**Publish\*WithRetry()** already exists to handle retries on Publish

## 5-Mock

It is advised to define an interface of what you will use from the broker in your program to allow easy mocking

# Tests

This repo includes integration tests that require a rabbitMQ to run.

```bash
docker run -p 15672:15672 -p 5672:5672 -d rabbitmq:3-management
# connect to localhost:15672 with guest:guest in your navigator to check it started
export AMQP_URI="amqp://localhost:5672"
# run tests
go test -tags=integration ./...
# docker stop ${docker_id_returned_by_docker_run}
```

# Potential future updates

- **ConsumeWithRetry()** using a backoff policy, so the consumer does not stop consuming at the first reconnection try if it fails
- Ability to add new labeled consumers to the broker, with different ConsumerConf (necessary to consume 2 queues with different QOS). We could then consume with the label of the wanted consumer
- helper tool to ack or nack a message with retries
- ack/nack outside of broker so we don't need the object when acking ?

# Name

amqpw originaly meant amqp worker, but the worker does not exist in the current version. Maybe it has become amqp wrapper ? Since it wraps it to simplify consumer and producer operations.
