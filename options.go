package amqpw

import (
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/consumer"
	"gitlab.com/qosenergy/amqpw/internal/producer"
)

/*
NewQOS defines the amount of messages RabbitMQ tries to lock for a consumer.
Defines the amount of messages locked before Ack for a consumer by rabbit.

See [Channel.QOS()] for details
(prefetchSize is forced to 0 as amqp091 does not handle it yet).

[Channel.QOS()]: https://pkg.go.dev/github.com/rabbitmq/amqp091-go#Channel.Qos
*/
func NewQOS(prefetchCount int) *common.QOS {
	return &common.QOS{
		PrefetchCount: prefetchCount,
		PrefetchSize:  0,
		Global:        false,
	}
}

/*
ConsumeOptions abstracts the optional options that can be passed to the [Broker.Consume] func.
If not given, all values are considered false/empty.
Use [NewConsumeOptions] to create for non default values.
*/
type ConsumeOptions struct {
	consumer.ConsumeOptions
}

// NewConsumeOptions is used to create the additional param to [Broker.Consume].
func NewConsumeOptions() *ConsumeOptions {
	return &ConsumeOptions{
		consumer.ConsumeOptions{
			Args: amqp091.Table{},
		},
	}
}

// SetAutoAck sets auto-ack to the given value instead of default false.
// autoAck active means every consumed message is instantly & automatically acked.
func (co *ConsumeOptions) SetAutoAck(autoAck bool) *ConsumeOptions {
	co.AutoAck = autoAck
	return co
}

// SetExclusive sets exclusive to the given value instead of default false.
// exclusive active means only one consumer can consume the target queue at a time.
// [Broker.Consume] will fail if another consumer exists (from RMQ's POV).
func (co *ConsumeOptions) SetExclusive(exclusive bool) *ConsumeOptions {
	co.Exclusive = exclusive
	return co
}

// SetNoWait sets no-wait to the given value instead of default false.
// noWait active means the consume func returns immediately, and the connection
// fails afterward if anything goes wrong. (/!\ this means bad config won't be detected)
func (co *ConsumeOptions) SetNoWait(noWait bool) *ConsumeOptions {
	co.NoWait = noWait
	return co
}

/*
SetArgs sets args to the given value instead of the default empty [amqp091.Table].
See amqp091 doc for allowed values in Table.

[amqp091.Table]: https://pkg.go.dev/github.com/rabbitmq/amqp091-go#Table
*/
func (co *ConsumeOptions) SetArgs(args amqp091.Table) *ConsumeOptions {
	co.Args = args
	return co
}

/*
ProduceOptions abstracts the optional options that can be passed to the Publish funcs.
If not given, all values are considered false/empty.
Use [NewProduceOptions] to create with non default values.
*/
type ProduceOptions struct {
	producer.ProduceOptions
}

/*
NewProduceOptions is used to create the additional param to
the following funcs if the default values need replacement:
  - [Broker.Publish]
  - [Broker.PublishASync]
  - [Broker.PublishWithRetry]
  - [Broker.PublishASyncWithRetry]
*/
func NewProduceOptions() *ProduceOptions {
	return &ProduceOptions{}
}

// SetMandatory sets mandatory to the given value instead of default false.
// Mandatory true means the publish will be rejected if the message could not be written
// in at least one queue.
func (po *ProduceOptions) SetMandatory(mandatory bool) *ProduceOptions {
	po.Mandatory = mandatory
	return po
}

/*
SetInfiniteRejectPublish sets infinite retry on reject-publish.
Be wary that if the topology is not properly configured, The publish will never end.
Check the statusChan of your Publish with
[gitlab.com/qosenergy/amqpw/error.IsErrPublishRejected]
to detect those.

For safety reasons, this option has no effect when statusChan is not defined
([Broker.Publish] & [Broker.PublishASync] are considered to always have undefined statusChan).
*/
func (po *ProduceOptions) SetInfiniteRejectPublish(infRejectPub bool) *ProduceOptions {
	po.InfiniteRejectPublish = infRejectPub
	return po
}
