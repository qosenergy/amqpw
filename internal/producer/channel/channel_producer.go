// Package channel regroups all that is handled at the scale of a single rabbitMQ channel
// (confirmation handler & channel producer)
package channel

import (
	"context"
	"errors"
	"fmt"

	"github.com/rabbitmq/amqp091-go"
	"golang.org/x/sync/errgroup"

	amqpwerror "gitlab.com/qosenergy/amqpw/error"
	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/producer/channel/confirmation"
	"gitlab.com/qosenergy/amqpw/internal/producer/smartmessage"
)

// Producer reads queries from the global producer's queryChan
// and tries to publish using it's onboarded channel
type Producer struct {
	ctx       context.Context
	reChannel common.ChannelReconnector

	// parentQueryChan used to consume queries to publish
	queryChan <-chan smartmessage.SmartPublish

	confirmationHandler *confirmation.Handler
}

// NewProducer creates a channel producer from a channel reconnector
func NewProducer(
	ctx context.Context,
	reChannel common.ChannelReconnector,
	queryChan <-chan smartmessage.SmartPublish,
) *Producer {
	cp := &Producer{
		ctx:       ctx,
		reChannel: reChannel,

		queryChan: queryChan,

		confirmationHandler: confirmation.NewHandler(ctx, reChannel.SendErr),
	}
	return cp
}

// Start starts the channel producer, which stops when the context is closed.
// When ctx is cancelled all pending confirmations are cancelled.
// /!\ the queryChan must remain open when this runs (it can republish)
func (cp *Producer) Start(errGrp *errgroup.Group) {
	cp.confirmationHandler.ListenConfirmations(errGrp)
	errGrp.Go(func() error {
		disconnected := true
		var currChan common.Channel
		for {
			select {
			case <-cp.ctx.Done():
				return nil
			case msg, ok := <-cp.queryChan:
				if !ok {
					return fmt.Errorf("unexpected queryChan close")
				}
				// handle (re)connection
				if disconnected {
					var chErr error
					currChan, chErr = cp.reChannel.Channel(nil, true)
					if chErr != nil {
						cp.reChannel.SendErr(fmt.Errorf("channel reconnection on publish: %w", chErr))
						if amqpwerror.IsErrRetriable(chErr) {
							msg.SubmitRetryBackOffEvent(fmt.Errorf("(re)connecting: %w", chErr))
							continue
						}
						msg.Callback(false, fmt.Errorf("channel reconnection on publish: %w", chErr))
						continue
					}
					disconnected = false
				}

				deferredConfirm, pubErr := msg.Publish(cp.ctx, currChan)
				if pubErr != nil {
					if errors.Is(pubErr, amqp091.ErrClosed) {
						// disconnected: must loop to recreate channel
						disconnected = true
						cp.confirmationHandler.Reset()
						// resubmit this message to be retried immediately
						// without incrementing msg retry (we technically did not try)
						msg.SubmitForPublish()
						continue
					}
					msg.SubmitRetryBackOffEvent(pubErr)
					continue
				}
				TOevt := msg.SubmitTOEvent(
					cp.confirmationHandler,
					deferredConfirm.DeliveryTag(),
				)
				cp.confirmationHandler.AddDeferredConfirmation(
					deferredConfirm,
					TOevt,
					msg,
				)
			}
		}
	})
}
