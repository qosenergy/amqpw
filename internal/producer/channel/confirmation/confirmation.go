// Package confirmation implements everything needed to handle amqp091.DeferredConfirmation
package confirmation

import (
	"context"
	"fmt"
	"io"
	"reflect"
	"sync"

	"gitlab.com/qosenergy/amqpw/internal/common"
	"golang.org/x/sync/errgroup"
)

var pool = sync.Pool{
	New: func() any {
		return &confirmAtion{}
	},
}

type toEvt interface {
	CheckSetDone() bool
}

type msg interface {
	Callback(bool, error)
	SubmitRetryBackOffEvent(error)
}

type errSender interface {
	SendErr(error)
}

// confirmAtion contains the deferredConfirmation & all knowledge on how to handle it
// through confirm() func
type confirmAtion struct {
	toEvt                toEvt
	msg                  msg
	deferredConfirmation common.DeferredConfirmation
	selectIndex          int
}

func (ca *confirmAtion) confirm(errSender errSender, cancel bool) {
	ack := ca.deferredConfirmation.Acked()
	if ca.toEvt.CheckSetDone() {
		// TO already proc: notify but do nothing
		if !cancel {
			errSender.SendErr(fmt.Errorf("received confirm (%t) for already Timouted message", ack))
		}
		return
	}
	if ack {
		ca.msg.Callback(true, nil)
		return
	}
	if cancel {
		ca.msg.Callback(false, fmt.Errorf("cancelled deffered confirmation: %w", io.EOF))
		return
	}

	ca.msg.SubmitRetryBackOffEvent(
		common.SetRejectPublishErr(
			fmt.Errorf("publish confirm rejected"),
		),
	)
}

// Handler handles deferredConfirmations
type Handler struct {
	ctx      context.Context
	sendErr  func(error)
	pendings map[uint64]*confirmAtion

	// contains selectCase slice (and way to find value in slice ?)
	selectSlice      []reflect.SelectCase // stored here only to avoid reallocation
	deliveryTagSlice []uint64
	addChan          chan *confirmAtion
	rmChan           chan uint64
	resetChan        chan struct{}
}

// NewHandler creates a confirmation handler
func NewHandler(ctx context.Context, sendErrFunc func(error)) *Handler {
	return &Handler{
		ctx:              ctx,
		sendErr:          sendErrFunc,
		pendings:         make(map[uint64]*confirmAtion, 0),
		selectSlice:      make([]reflect.SelectCase, 0, selectHeaderSize),
		deliveryTagSlice: make([]uint64, 0),
		addChan:          make(chan *confirmAtion),
		rmChan:           make(chan uint64),
		resetChan:        make(chan struct{}),
	}
}

// String redefined to avoid race condition in mock
func (*Handler) String() string {
	return "Handler"
}

// SendErr sends the error using the func given in NewHandler
func (ch *Handler) SendErr(err error) {
	ch.sendErr(err)
}

// ListenConfirmations starts the routine that handles all confirmations.
// It stops on ctx close.
func (ch *Handler) ListenConfirmations(errGrp *errgroup.Group) {
	ch.initConfirmation()
	errGrp.Go(func() error {
		for {
			done := ch.selectConfirmation()
			if done {
				ch.cancelAll()
				return nil
			}
		}
	})
}

const selectHeaderSize = 4

func (ch *Handler) initConfirmation() {
	ctx := ch.ctx
	ch.selectSlice = append(
		ch.selectSlice,
		reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ctx.Done())},
		reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch.addChan)},
		reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch.rmChan)},
		reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(ch.resetChan)},
	)
}

// selectConfirmation generate a select from the pending confirmAtions.
// It returns once the select procs
// It is meant to run in a loop and regenerate the select Case everytime
func (ch *Handler) selectConfirmation() (done bool) {
	chosen, val, _ := reflect.Select(ch.selectSlice)
	switch chosen {
	case 0: // ctx done
		return true
	case 1: // add
		newConfirmAtion := val.Interface().(*confirmAtion)
		ch.handleAdd(newConfirmAtion)
	case 2: // rm
		deliveryTag := val.Interface().(uint64)
		confirmA, ok := ch.pendings[deliveryTag]
		if ok {
			// can happen since RM comes from outside: fire & RM call can cause double deletion (segfaults)
			ch.handleRm(confirmA)
		}
	case 3: // reset
		ch.resetAll()
	default: // a deferredConfirmation fired
		deliveryTag := ch.deliveryTagSlice[chosen-selectHeaderSize]
		confirmA := ch.pendings[deliveryTag]
		// so we confirm
		confirmA.confirm(ch, false)
		// then rm the handled confirmation
		ch.handleRm(confirmA)
	}
	return false
}

func (ch *Handler) handleAdd(newConfirmAtion *confirmAtion) {
	dTag := newConfirmAtion.deferredConfirmation.DeliveryTag()
	ch.pendings[dTag] = newConfirmAtion
	newConfirmAtion.selectIndex = len(ch.selectSlice)
	ch.deliveryTagSlice = append(ch.deliveryTagSlice, dTag)
	ch.selectSlice = append(ch.selectSlice, reflect.SelectCase{
		Dir:  reflect.SelectRecv,
		Chan: reflect.ValueOf(newConfirmAtion.deferredConfirmation.Done()),
	})
}

func (ch *Handler) handleRm(ca *confirmAtion) {
	selectIndex := ca.selectIndex
	lastSelectSliceID := len(ch.selectSlice) - 1
	tagID, lastTagID := selectIndex-selectHeaderSize, lastSelectSliceID-selectHeaderSize
	// switch with last if it's not last
	if selectIndex != lastSelectSliceID {
		// switch with last element of select slice (to later remove it)
		ch.selectSlice[selectIndex], ch.selectSlice[lastSelectSliceID] =
			ch.selectSlice[lastSelectSliceID], ch.selectSlice[selectIndex]
		// switch with last element of deliveryTag slice  (to later remove it)
		ch.deliveryTagSlice[tagID], ch.deliveryTagSlice[lastTagID] =
			ch.deliveryTagSlice[lastTagID], ch.deliveryTagSlice[tagID]
		switchedDeliveryTag := ch.deliveryTagSlice[tagID]
		// update switched element index
		ch.pendings[switchedDeliveryTag].selectIndex = selectIndex
	}
	// then reduce slices size by 1
	ch.selectSlice = ch.selectSlice[:lastSelectSliceID]
	ch.deliveryTagSlice = ch.deliveryTagSlice[:lastTagID]
	// delete
	dTag := ca.deferredConfirmation.DeliveryTag()
	donePending := ch.pendings[dTag]
	delete(ch.pendings, dTag)
	donePending.deferredConfirmation = nil
	donePending.msg = nil
	donePending.toEvt = nil
	pool.Put(donePending)
}

// AddDeferredConfirmation adds a new DeferredConfirmation to the handler,
// alongside a function that will be called on confirmation.
func (ch *Handler) AddDeferredConfirmation(
	deferredConfirm common.DeferredConfirmation,
	toEvt toEvt,
	msg msg,
) {
	ca := pool.Get().(*confirmAtion)
	ca.deferredConfirmation = deferredConfirm
	ca.msg = msg
	ca.toEvt = toEvt
	select {
	case <-ch.ctx.Done():
		ca.confirm(ch, true)
	case ch.addChan <- ca:
	}
}

// RmDeferredConfirmation to stop waiting for a confirmation (does not trigger call to confirmFunc)
func (ch *Handler) RmDeferredConfirmation(deliveryTag uint64) {
	select {
	case <-ch.ctx.Done():
		return
	case ch.rmChan <- deliveryTag:
	}
}

// Reset to stop waiting for a confirmation (does not trigger call to confirmFunc)
func (ch *Handler) Reset() {
	select {
	case <-ch.ctx.Done():
		return
	case ch.resetChan <- struct{}{}:
	}
}

// resetAll after channel reconnect (retry all retriable messages)
func (ch *Handler) resetAll() {
	for k, val := range ch.pendings {
		val.confirm(ch, false)
		delete(ch.pendings, k)
	}
	ch.selectSlice = ch.selectSlice[:selectHeaderSize]
	ch.deliveryTagSlice = ch.deliveryTagSlice[:0]
}

// cancelAll unacks all messages (ctx cancel: closing)
func (ch *Handler) cancelAll() {
	for k, val := range ch.pendings {
		val.confirm(ch, true)
		delete(ch.pendings, k)
	}
	ch.selectSlice = ch.selectSlice[:selectHeaderSize]
	ch.deliveryTagSlice = ch.deliveryTagSlice[:0]
}
