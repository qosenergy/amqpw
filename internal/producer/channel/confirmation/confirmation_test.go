package confirmation

// revive:disable:dot-imports

import (
	"context"
	"sort"
	"sync"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/test"
	"golang.org/x/sync/errgroup"
)

// revive:enable:dot-imports

var (
	TRUE  = true
	FALSE = false
)

func TestSelectConfirmation(t *testing.T) {
	Convey("Confirmation handler", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(cmc.Clean)
		Convey("When running selectConfirmation", func() {
			ctx, cancel := context.WithCancel(context.Background())
			handler := NewHandler(ctx, func(error) {})
			Convey("on context Done", func() {
				handler.initConfirmation()
				cancel()
				SoMsg("returns with \"done\" true", handler.selectConfirmation(), ShouldBeTrue)
			})
			Convey("on Add", func() {
				handler.initConfirmation()
				var (
					wg     sync.WaitGroup
					isDone bool
				)
				wg.Add(1)
				go func() {
					isDone = handler.selectConfirmation()
					wg.Done()
				}()
				mockEvt := NewMocktoEvt(cmc)
				mockMsg := NewMockmsg(cmc)
				mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)
				mockDeferredConfirm.On("DeliveryTag").Return(uint64(2)).Once()
				mockDeferredConfirm.On("Done").Return((<-chan struct{})(make(chan struct{}))).Once()
				handler.AddDeferredConfirmation(mockDeferredConfirm, mockEvt, mockMsg)
				wg.Wait()
				SoMsg("returnes with done false", isDone, ShouldBeFalse)
				SoMsg("... and with 5 values in select", len(handler.selectSlice), ShouldEqual, 5)
			})
			Convey("on Rm", func() {
				handler.initConfirmation()
				var (
					wg     sync.WaitGroup
					isDone bool
				)
				wg.Add(1)
				go func() {
					// for Add
					handler.selectConfirmation()
					// for Rm
					isDone = handler.selectConfirmation()
					wg.Done()
				}()
				mockEvt := NewMocktoEvt(cmc)
				mockMsg := NewMockmsg(cmc)
				mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)
				var dTag uint64 = 2
				mockDeferredConfirm.On("DeliveryTag").Return(dTag).Twice()
				mockDeferredConfirm.On("Done").Return((<-chan struct{})(make(chan struct{}))).Once()
				handler.AddDeferredConfirmation(mockDeferredConfirm, mockEvt, mockMsg)
				handler.RmDeferredConfirmation(dTag)
				wg.Wait()
				SoMsg("returns with done false", isDone, ShouldBeFalse)
				SoMsg("... and with 4 values in select", len(handler.selectSlice), ShouldEqual, 4)
			})
			Convey("on Reset", func() {
				handler.initConfirmation()
				var (
					wg     sync.WaitGroup
					isDone bool
				)
				wg.Add(1)
				go func() {
					// for Add
					handler.selectConfirmation()
					// for Add 2
					handler.selectConfirmation()
					// for Reset
					isDone = handler.selectConfirmation()
					wg.Done()
				}()
				mockEvt := NewMocktoEvt(cmc)
				mockMsg := NewMockmsg(cmc)
				mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)
				mockDeferredConfirm.On("DeliveryTag").Return(uint64(2)).Once()
				mockDeferredConfirm.On("Done").Return((<-chan struct{})(make(chan struct{}))).Once()
				handler.AddDeferredConfirmation(mockDeferredConfirm, mockEvt, mockMsg)

				mockEvt2 := NewMocktoEvt(cmc)
				mockMsg2 := NewMockmsg(cmc)
				mockDeferredConfirm2 := common.NewMockDeferredConfirmation(cmc)
				mockDeferredConfirm2.On("DeliveryTag").Return(uint64(3)).Once()
				mockDeferredConfirm2.On("Done").Return((<-chan struct{})(make(chan struct{}))).Once()
				handler.AddDeferredConfirmation(mockDeferredConfirm2, mockEvt2, mockMsg2)

				mockDeferredConfirm.On("Acked").Return(false).Once()
				mockEvt.On("CheckSetDone").Return(false).Once()
				mockMsg.On("SubmitRetryBackOffEvent", mock.Anything).Once()
				mockDeferredConfirm2.On("Acked").Return(false).Once()
				mockEvt2.On("CheckSetDone").Return(true).Once()
				var err error
				handler.sendErr = func(sendErr error) { err = sendErr }
				handler.Reset()
				wg.Wait()
				SoMsg("returns with done false", isDone, ShouldBeFalse)
				SoMsg("... with error sent", err, ShouldNotBeNil)
				SoMsg("... and with 4 values in select", len(handler.selectSlice), ShouldEqual, 4)
			})
		})
		Convey("When running ListenConfirmations routine", func() {
			ctx, cancel := context.WithCancel(context.Background())
			errGrp, ctx := errgroup.WithContext(ctx)
			handler := NewHandler(ctx, func(error) {})
			handler.ListenConfirmations(errGrp)
			type confirmed struct {
				id  uint64
				ack bool
			}

			Convey("Add, Rm & Reset", func() {
				confirmeds := make([]confirmed, 0)
				n := 10
				doneChans := make([]chan<- struct{}, n)
				waitAction := make(chan struct{}, 1)
				for i := 0; i < n; i++ {
					n := uint64(i)
					mockToEvt := NewMocktoEvt(cmc)
					var ackResult *bool
					mockMsg := NewMockmsg(cmc)
					switch n {
					case 0:
						// for RmDeferredConfirmation (nothing to do)
					case 3, 5, 9:
						// simulate DeferredConfirmation proc with true ack
						ackResult = &TRUE
						mockToEvt.On("CheckSetDone").Return(false).Once()
						mockMsg.On("Callback", true, (error)(nil)).Run(func(mock.Arguments) {
							confirmeds = append(confirmeds, confirmed{id: n, ack: true})
							waitAction <- struct{}{}
						}).Once()
					case 6:
						// simulate DeferredConfirmation proc with false ack
						ackResult = &FALSE
						mockToEvt.On("CheckSetDone").Return(false).Once()
						mockMsg.On("SubmitRetryBackOffEvent", mock.Anything).Run(func(mock.Arguments) {
							confirmeds = append(confirmeds, confirmed{id: n, ack: false})
							waitAction <- struct{}{}
						}).Once()
					default:
						// simulate reset by ChannelProducer
						ackResult = &FALSE
						mockToEvt.On("CheckSetDone").Return(false).Once()
						mockMsg.On("SubmitRetryBackOffEvent", mock.Anything).Run(func(mock.Arguments) {
							confirmeds = append(confirmeds, confirmed{id: n, ack: false})
							waitAction <- struct{}{}
						}).Once()
					}
					var confirm *common.MockDeferredConfirmation
					confirm, doneChans[i] = NewDeferredConfirm(cmc, n, ackResult)

					handler.AddDeferredConfirmation(
						confirm,
						mockToEvt,
						mockMsg,
					)
				}

				close(doneChans[3])
				<-waitAction
				close(doneChans[6])
				<-waitAction
				close(doneChans[5])
				<-waitAction
				close(doneChans[9])
				<-waitAction
				handler.RmDeferredConfirmation(0)

				SoMsg("when we receive confirmation, callback is done in proper order with proper value",
					confirmeds, ShouldEqual, []confirmed{
						{id: 3, ack: true},
						{id: 6, ack: false},
						{id: 5, ack: true},
						{id: 9, ack: true},
					})
				confirmeds = confirmeds[:0]

				go handler.Reset()
				// wait 6 remaining msgs from resetAll (on 10, 1 was removed & 4 confirmed)
				<-waitAction
				<-waitAction
				<-waitAction
				<-waitAction
				<-waitAction

				sort.Slice(confirmeds, func(i, j int) bool {
					return confirmeds[i].id < confirmeds[j].id
				})
				SoMsg("when we reset the ConfirmationHandler: callback are properly called",
					confirmeds, ShouldEqual, []confirmed{
						{id: 1, ack: false},
						{id: 2, ack: false},
						{id: 4, ack: false},
						{id: 7, ack: false},
						{id: 8, ack: false},
					})
				cancel()
			})

			Convey("Add & Cancel", func() {
				confirmeds := make([]confirmed, 0)
				waitAction := make(chan struct{}, 1)

				confirm, _ := NewDeferredConfirm(cmc, 1, &FALSE)
				mockToEvt := NewMocktoEvt(cmc)
				mockToEvt.On("CheckSetDone").Return(false)
				mockMsg := NewMockmsg(cmc)
				mockMsg.On("Callback", false, mock.Anything).Run(func(mock.Arguments) {
					confirmeds = append(confirmeds, confirmed{id: 42, ack: false})
					waitAction <- struct{}{}
				}).Once()
				handler.AddDeferredConfirmation(
					confirm,
					mockToEvt,
					mockMsg,
				)
				confirm, _ = NewDeferredConfirm(cmc, 42, &FALSE)
				mockToEvt2 := NewMocktoEvt(cmc)
				mockToEvt2.On("CheckSetDone").Return(false)
				mockMsg2 := NewMockmsg(cmc)
				mockMsg2.On("Callback", false, mock.Anything).Run(func(mock.Arguments) {
					confirmeds = append(confirmeds, confirmed{id: 1, ack: false})
					waitAction <- struct{}{}
				}).Once()

				handler.AddDeferredConfirmation(
					confirm,
					mockToEvt2,
					mockMsg2,
				)

				cancel()
				<-waitAction
				<-waitAction
				SoMsg("when we cancel: errGroup terminates without error", errGrp.Wait(), ShouldBeNil)

				sort.Slice(confirmeds, func(i, j int) bool {
					return confirmeds[i].id < confirmeds[j].id
				})
				SoMsg("and we get expected callbacks unack from cancel",
					confirmeds, ShouldEqual, []confirmed{
						{id: 1, ack: false},
						{id: 42, ack: false},
					})
			})

			Convey("Fire & Rm conflict", func() {
				waitAction := make(chan struct{}, 1)
				confirm, doneChan := NewDeferredConfirm(cmc, 1, &TRUE)
				mockToEvt := NewMocktoEvt(cmc)
				mockToEvt.On("CheckSetDone").Return(false)
				mockMsg := NewMockmsg(cmc)
				mockMsg.On("Callback", true, mock.Anything).Run(func(mock.Arguments) {
					waitAction <- struct{}{}
				}).Once()
				handler.AddDeferredConfirmation(
					confirm,
					mockToEvt,
					mockMsg,
				)
				// fire confirmation
				close(doneChan)
				<-waitAction

				// then rm it (no "So": panics if wrong)
				handler.RmDeferredConfirmation(1)
			})
		})
	})
}

func NewDeferredConfirm(
	cmc *test.ConveyMockCleanup, id uint64, ack *bool,
) (
	*common.MockDeferredConfirmation,
	chan<- struct{},
) {
	deferredConfirm := common.NewMockDeferredConfirmation(cmc)
	deferredConfirm.On("DeliveryTag").Return(id)
	done := make(chan struct{})
	deferredConfirm.On("Done").Return(
		(<-chan struct{})(done),
	).Once()

	if ack != nil {
		deferredConfirm.On("Acked").Return(*ack).Once()
	}
	return deferredConfirm, done
}
