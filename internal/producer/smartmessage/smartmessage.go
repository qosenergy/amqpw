// Package smartmessage exposes a message struct with knowledge on it's own life cycle in the producer
package smartmessage

import (
	"context"
	"fmt"
	"sync"
	"time"

	backoff "github.com/cenkalti/backoff/v4"
	"github.com/rabbitmq/amqp091-go"
	amqpwerror "gitlab.com/qosenergy/amqpw/error"
	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/producer/smartmessage/event"
)

var pool = sync.Pool{
	New: func() any {
		return &smartPublish{}
	},
}

// InfiniteRejectPublishBackoff defined the time to wait between retries on reject publish
// when and only when infinite retry on reject publishes is on. Otherwise, default backoff is used.
// Only modified in tests
var InfiniteRejectPublishBackoff = 10 * time.Second

// SmartPublish handles all interactions between producer sub-parts,
// including publishing, submiting events & callback.
// It is used to dispatch charge between channelProducers, trigger Events, etc.
type SmartPublish interface {
	// Callback calls the callback func given in new & frees the object in pool
	Callback(ack bool, err error)

	// StatusErr sends the given error to the statusChan given in the producer.Publish (if any)
	StatusErr(err error)

	// SubmitForPublish send the message to the producer's submit chan so it is published
	SubmitForPublish()

	// SubmitRetryBackOffEvent adds a retry event for this message to the event manager
	// if some backoff remains. Otherwise, it calls the nack callback.
	// It also always sends the given error to the status chan.
	SubmitRetryBackOffEvent(reason error)

	// SubmitTOEvent adds a TO event to the event manager.
	// It returns the created event so we can mark it as obsolete on publish confirmation.
	// The given function will be call if/when the event procs.
	SubmitTOEvent(confirmHandler event.ConfirmationHandler, deliveryTag uint64) *event.TOEvent

	// Publish is used by the channel producer. It publishes the message to rmq with the given amqp.Channel.
	Publish(ctx context.Context, channel common.Channel) (common.DeferredConfirmation, error)
}

// smartPublish contains all data required to publish, submit events & callback.
// It serves as a bridge between the producer and the event manager
type smartPublish struct {
	// async callback
	callback func(bool, error)
	// publish query
	exchange   string
	routingKey string
	msg        amqp091.Publishing
	// publish retry
	retriesDone  int
	retryBackoff backoff.BackOff
	toDuration   time.Duration
	// event manager
	evtManager *event.Manager
	// publish main chan
	queryChan  chan<- SmartPublish
	statusChan chan<- error
	// publish options
	mandatory         bool
	infiniteRejectPub bool
	// state
	state state
}

type state string

const (
	pendingPublish state = "pending publish"
	pendingConfirm state = "pending confirm"
	pendingRetry   state = "pending retry"
	publishing     state = "publishing"
	doneState      state = "done"
)

// NewSmartPublish creates a new smartPublish (from a sync.Pool for efficiency)
func NewSmartPublish(
	exchange, routingKey string, msg amqp091.Publishing,
	timeoutDuration time.Duration,
	retryBackoff backoff.BackOff,
	callback func(bool, error),
	evtManager *event.Manager,
	queryChan chan<- SmartPublish,
	statusChan chan<- error,
	mandatory bool,
	infiniteRejectPublish bool,
) SmartPublish {
	sp := pool.Get().(*smartPublish)
	sp.callback = callback

	sp.exchange = exchange
	sp.routingKey = routingKey
	sp.msg = msg

	sp.retriesDone = 0
	sp.retryBackoff = retryBackoff
	sp.toDuration = timeoutDuration

	sp.evtManager = evtManager

	sp.queryChan = queryChan
	sp.statusChan = statusChan

	sp.mandatory = mandatory
	sp.infiniteRejectPub = infiniteRejectPublish
	return sp
}

// Callback calls the callback func given in new & frees the object in pool
func (sp *smartPublish) Callback(ack bool, err error) {
	sp.state = doneState
	sp.callback(ack, err)
	pool.Put(sp)
}

// StatusErr sends the given error to the statusChan given in the producer.Publish (if any)
func (sp *smartPublish) StatusErr(err error) {
	select {
	case sp.statusChan <- err:
	default:
	}
}

// SubmitForPublish send the message to the producer's submit chan so it is published
func (sp *smartPublish) SubmitForPublish() {
	sp.state = pendingPublish
	sp.queryChan <- sp
}

// SubmitRetryBackOffEvent adds a retry event for this message to the event manager
// if some backoff remains. Otherwise, it calls the nack callback.
// It also always sends the given error to the status chan.
func (sp *smartPublish) SubmitRetryBackOffEvent(reason error) {
	sp.state = pendingRetry
	statusErr := reason
	isRetriable := amqpwerror.IsErrRetriable(reason)
	var nextBackOff time.Duration
	if sp.infiniteRejectPub && common.IsErrPublishRejected(reason) && sp.statusChan != nil {
		statusErr = fmt.Errorf("(infinite retry on reject publish): %w", statusErr)
		nextBackOff = InfiniteRejectPublishBackoff
	} else {
		nextBackOff = sp.retryBackoff.NextBackOff()
		if nextBackOff == backoff.Stop || !isRetriable {
			sp.Callback(false, fmt.Errorf(
				"failure after %d retries (no more retry), last error: %w",
				sp.retriesDone, reason,
			))
			return
		}
	}
	sp.StatusErr(fmt.Errorf("publish error (try %d): %w", sp.retriesDone, statusErr))

	sp.retriesDone++
	sp.evtManager.AddEvent(
		event.NewRetryEvent(
			time.Now().Add(nextBackOff),
			sp,
		),
	)
}

// SubmitTOEvent adds a TO event to the event manager.
// It returns the created event so we can mark it as obsolete on publish confirmation.
// The given function will be call if/when the event procs.
func (sp *smartPublish) SubmitTOEvent(confirmHandler event.ConfirmationHandler, deliveryTag uint64) *event.TOEvent {
	sp.state = pendingConfirm
	evt := event.NewTOEvent(
		time.Now().Add(sp.toDuration),
		deliveryTag,
		confirmHandler,
		sp,
	)
	sp.evtManager.AddEvent(evt)
	return evt
}

// Publish is used by the channel producer. It publishes the message to rmq with the given amqp.Channel.
func (sp *smartPublish) Publish(
	ctx context.Context, amqpChannel common.Channel,
) (common.DeferredConfirmation, error) {
	sp.state = publishing
	deferredConfirm, pubErr := amqpChannel.PublishWithDeferredConfirmWithContext(ctx, sp.exchange, sp.routingKey,
		sp.mandatory, false,
		sp.msg)
	return deferredConfirm, common.NotNilWrap("smart message Publish: %w", pubErr, common.SetRetriableErr)
}

// String strigifies smart messages
func (sp *smartPublish) String() string {
	return fmt.Sprintf("exchange: %s, rk: %s, state: %s", sp.exchange, sp.routingKey, sp.state)
}
