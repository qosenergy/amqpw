// Package event exposes an event ("publish timeout" or "retry with delay") handler
package event

import (
	"container/heap"
	"context"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"
)

// Manager manages events and fires them whenever needed
type Manager struct {
	eventHeap   *evtHeap
	mx          sync.Mutex
	heapUpdated chan struct{}
}

// NewManager creates a new Manager
func NewManager() *Manager {
	h := make(evtHeap, 0)
	heap.Init(&h)
	return &Manager{
		heapUpdated: make(chan struct{}, 1),

		eventHeap: &h,
	}
}

// AddEvent adds the given event to the manager so it is triggered when it's time comes
func (em *Manager) AddEvent(evt Event) {
	em.mx.Lock()
	evt.setEvtManager(em)
	heap.Push(em.eventHeap, evt)
	em.mx.Unlock()

	select {
	case em.heapUpdated <- struct{}{}:
	default:
	}
}

// rmEvent removes a given event from the heap & maintains order (only used internally by event package)
func (em *Manager) rmEvent(evt Event) {
	em.mx.Lock()
	// no check is done on id : the event lib ensures the value is legal when calling rmEvent
	id := evt.getHeapID()
	// Remove also calls Pop (which sets id to -1)
	heap.Remove(em.eventHeap, id)
	evt.setEvtManager(nil)
	em.mx.Unlock()

	select {
	case em.heapUpdated <- struct{}{}:
	default:
	}
}

// Start starts handling events until the given context is closed
func (em *Manager) Start(ctx context.Context, errGrp *errgroup.Group) {
	errGrp.Go(func() error {
		timer := time.NewTimer(time.Hour)
		timer.Stop()
		var timerOn bool

		for {
			em.mx.Lock()
			if em.eventHeap.Len() > 0 {
				timerOn = true
				timer.Reset(time.Until(em.eventHeap.Peek().when()))
			}
			em.mx.Unlock()
			select {
			case <-ctx.Done():
				timer.Stop()
				em.mx.Lock()
				heapLen := em.eventHeap.Len()
				for i := 0; i < heapLen; i++ {
					currEvt := heap.Pop(em.eventHeap).(Event)
					currEvt.cancel()
					currEvt.setEvtManager(nil)
				}
				em.mx.Unlock()
				return nil
			case <-timer.C:
				timerOn = false
				em.mx.Lock()
				currEvt := heap.Pop(em.eventHeap).(Event)
				currEvt.setEvtManager(nil)
				em.mx.Unlock()
				currEvt.do()
			case <-em.heapUpdated:
				if !timer.Stop() && timerOn {
					<-timer.C
				}
				timerOn = false
			}
		}
	})
}
