// Package producer implements the tool to publish messages to rabbitmq with retries, reconnection & async
package producer

import (
	"context"
	"errors"
	"fmt"
	"io"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/qosenergy/amqpw/internal/common"
	channelproducer "gitlab.com/qosenergy/amqpw/internal/producer/channel"
	"gitlab.com/qosenergy/amqpw/internal/producer/smartmessage"
	"gitlab.com/qosenergy/amqpw/internal/producer/smartmessage/event"
	"golang.org/x/sync/errgroup"
)

const (
	defaultConfirmationTimeout     = 10 * time.Second
	defaultMaxPendingConfirmations = 200
)

// Conf is the required conf for a producer.
// ChannelPoolSize will be considered 1 if the given value is smaller than 1.
// ChannelPoolSize should also not be too big (rabbitMQ has it's limitations)
// ConfirmationTimeout defaults to 10s if set to 0
// MaxPendingConfirmations defaults to 200 if set to 0
type Conf struct {
	ChannelPoolSize         int
	ConfirmationTimeout     time.Duration
	ForcePersistence        bool
	ForceMandatory          bool
	ForceInfRejectPub       bool
	MaxPendingConfirmations int
}

// ProduceOptions abstracts the optional options that can be passed to the Publish functions.
// If not given, all values are considered false/empty
type ProduceOptions struct {
	Mandatory             bool
	InfiniteRejectPublish bool
}

/*
RabbitMQProducer implements Producer interface and represents an amqp channel.
Caller can use this channel to publish on several exchanges and each exchange will be created at runtime if needed.
For convenience, it is possible to define a default exchange/queue couple at initialization as well.
*/
type RabbitMQProducer struct {
	ctx context.Context
	// parameter fields

	forcePersistence    bool
	forceMandatory      bool
	forceInfRejectPub   bool
	confirmationTimeout time.Duration

	queryChan    chan smartmessage.SmartPublish
	aSyncLimiter chan struct{}

	// closing
	ongoingWG sync.WaitGroup
	errGrp    *errgroup.Group
	cancel    func()
	once      sync.Once

	eventManager *event.Manager
}

// NewRabbitMQProducer creates a new RMQ producer
func NewRabbitMQProducer(
	ctx context.Context,
	conn common.ConnReconnector,
	config Conf,
) *RabbitMQProducer {
	cancellableCtx, cancel := context.WithCancel(context.WithoutCancel(ctx))
	errGrp, producerCtx := errgroup.WithContext(cancellableCtx)

	timeout := config.ConfirmationTimeout
	if timeout == 0 {
		timeout = defaultConfirmationTimeout
	}
	poolSize := 1
	if config.ChannelPoolSize > 1 {
		poolSize = config.ChannelPoolSize
	}

	maxPendingConfirmations := defaultMaxPendingConfirmations
	if config.MaxPendingConfirmations > 0 {
		maxPendingConfirmations = config.MaxPendingConfirmations
	}

	producer := &RabbitMQProducer{
		ctx: producerCtx,

		confirmationTimeout: timeout,
		forcePersistence:    config.ForcePersistence,
		forceMandatory:      config.ForceMandatory,
		forceInfRejectPub:   config.ForceInfRejectPub,

		queryChan:    make(chan smartmessage.SmartPublish, maxPendingConfirmations),
		aSyncLimiter: make(chan struct{}, maxPendingConfirmations),

		eventManager: event.NewManager(),
		errGrp:       errGrp,
		cancel:       cancel,
	}

	producer.eventManager.Start(producerCtx, producer.errGrp)

	for i := 0; i < poolSize; i++ {
		reChan := conn.NewReChannel()
		channelProducer := channelproducer.NewProducer(
			producerCtx,
			reChan, producer.queryChan,
		)

		channelProducer.Start(producer.errGrp)
	}

	return producer
}

// Wait waits all messages in production to finish (success of failure)
func (p *RabbitMQProducer) Wait() {
	p.ongoingWG.Wait()
}

// Close cancels all messages being produced (callback(false,io.EOF)), then stops all goroutines.
// It can be used to retrieve the error that caused the producer to stop working, if it closed by itself.
func (p *RabbitMQProducer) Close() error {
	p.cancel()
	closeErr := p.errGrp.Wait()
	p.once.Do(func() {
		go func() {
			p.ongoingWG.Wait()
			close(p.queryChan)
		}()
		for msg := range p.queryChan {
			msg.Callback(false, fmt.Errorf("producer closing: %w", io.EOF))
		}
	})
	return common.NotNilWrap("producer internal error: %w", closeErr)
}

/*
Publish synchronously sends a message to an exchange which must have been declared before.
If not, publish will fail and channel going to be closed.
It returns a nil error once write is confirmed by the server or a confirmation error otherwise.
*/
func (p *RabbitMQProducer) Publish(
	exchange string, routingKey string, publishing amqp091.Publishing,
	options *ProduceOptions,
) error {
	return p.PublishWithRetry(
		exchange,
		routingKey,
		publishing,
		&backoff.StopBackOff{},
		nil,
		options,
	)
}

// PublishWithRetry calls Publish until it succeeds or the number of retries defined
// in the strategy is reached.
func (p *RabbitMQProducer) PublishWithRetry(
	exchange string,
	routingKey string,
	publishing amqp091.Publishing,
	retryBackoff backoff.BackOff,
	statusChan chan error,
	options *ProduceOptions,
) error {
	internalCallbackFn, internalCallbackChan := buildSyncInternalCallback()

	p.PublishASyncWithRetry(
		exchange,
		routingKey,
		publishing,
		internalCallbackFn,
		retryBackoff,
		statusChan,
		options,
	)

	if err := <-internalCallbackChan; err != nil {
		return fmt.Errorf("publishing synchronously: %w", err)
	}

	return nil
}

// PublishASync works the same as Publish but returns before receiving confirmation from RabbitMQ.
// The callback will be called once the publishing confirmed or expired.
func (p *RabbitMQProducer) PublishASync(
	exchange string,
	routingKey string,
	publishing amqp091.Publishing,
	callback func(bool, error),
	options *ProduceOptions,
) {
	p.PublishASyncWithRetry(
		exchange,
		routingKey,
		publishing,
		callback,
		&backoff.StopBackOff{},
		nil,
		options,
	)
}

/*
PublishASyncWithRetry calls PublishASync until it succeeds to publish and the delivery has been confirmed
or if the number of retries defined in the strategy is reached.

  - retryBackoff is used to retry amqp publish.
  - statusChan will receive an error to log every time a retry is done (the error that led to the retry).
    statusChan can be nil.
*/
func (p *RabbitMQProducer) PublishASyncWithRetry(
	exchange string,
	routingKey string,
	publishing amqp091.Publishing,
	callback func(bool, error),
	retryBackoff backoff.BackOff,
	statusChan chan error,
	options *ProduceOptions,
) {
	p.ongoingWG.Add(1)
	var done bool
	select {
	case <-p.ctx.Done():
		done = true
	default:
	}
	select {
	case <-p.ctx.Done():
		done = true
	case p.aSyncLimiter <- struct{}{}:
		// wait for number of ongoing requests to be under a threshold (anti-deadlock)
	}

	if done {
		// callback call MUST be async
		p.ongoingWG.Done()
		go callback(false, io.EOF)
		return
	}

	wrappedCallback := func(ack bool, err error) {
		if common.IsErrPublishRejected(err) {
			// if this is a reject publish, it is not really an error
			err = nil
		}
		p.ongoingWG.Done()
		callback(ack, err)
		<-p.aSyncLimiter
	}

	if p.forcePersistence {
		publishing.DeliveryMode = amqp091.Persistent
	}
	var (
		mandatory    bool
		infRejectPub bool
	)
	if options != nil {
		mandatory = options.Mandatory
	}
	if p.forceMandatory {
		mandatory = true
	}
	if options != nil {
		infRejectPub = options.InfiniteRejectPublish
	}
	if p.forceInfRejectPub {
		infRejectPub = true
	}
	query := smartmessage.NewSmartPublish(
		exchange,
		routingKey,
		publishing,
		p.confirmationTimeout,
		retryBackoff,
		wrappedCallback,
		p.eventManager,
		p.queryChan,
		statusChan,
		mandatory,
		infRejectPub,
	)

	// send query in retry loop
	query.SubmitForPublish()
}

func buildSyncInternalCallback() (func(bool, error), <-chan error) {
	internalCallbackChan := make(chan error)
	return func(ack bool, err error) {
		if ack {
			internalCallbackChan <- nil
			return
		}
		if err != nil {
			internalCallbackChan <- fmt.Errorf("error publishing message: %w", err)
			return
		}
		internalCallbackChan <- common.SetRejectPublishErr(errors.New("publishing not acknowledged"))
	}, internalCallbackChan
}
