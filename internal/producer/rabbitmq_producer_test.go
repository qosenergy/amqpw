package producer

// revive:disable:dot-imports

import (
	"context"
	"errors"
	"fmt"
	"io"
	"testing"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/rabbitmq/amqp091-go"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"

	amqpwerror "gitlab.com/qosenergy/amqpw/error"
	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/producer/smartmessage"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

// revive:enable:dot-imports

func TestNewRabbitMQProducer(t *testing.T) {
	Convey("Producer New", t, func(c C) {
		ctx, cancel := context.WithCancel(context.Background())
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(func() {
			cmc.Clean()
		})
		mockReConn := common.NewMockConnReconnector(cmc)
		Convey("with default conf", func() {
			mockReChan := common.NewMockChannelReconnector(cmc)
			mockReConn.On("NewReChannel").Return(mockReChan).Once()
			producer := NewRabbitMQProducer(ctx, mockReConn, Conf{})
			SoMsg("producer is properly created", producer, ShouldNotBeNil)
			SoMsg("with correct confirmation timeout",
				producer.confirmationTimeout, ShouldEqual, defaultConfirmationTimeout,
			)
			SoMsg("and correct force persistence", producer.forcePersistence, ShouldBeFalse)
			SoMsg("and correct force mandatory", producer.forceMandatory, ShouldBeFalse)
			SoMsg("and correct force infinite reject publish", producer.forceInfRejectPub, ShouldBeFalse)

			var waitChan = make(chan struct{})
			go func() {
				_ = producer.errGrp.Wait()
				close(waitChan)
			}()
			cancel()
			// give it time to stop
			time.Sleep(5 * time.Millisecond)
			running := true
			select {
			case <-waitChan:
				running = false
			default:
			}
			SoMsg("and it still runs after closing parent context", running, ShouldBeTrue)

			closeErr := producer.Close()
			<-waitChan

			SoMsg("and it properly stops on Close with no error", closeErr, ShouldBeNil)
		})
		Convey("with specific conf", func() {
			mockReChan := common.NewMockChannelReconnector(cmc)
			mockReConn.On("NewReChannel").Return(mockReChan).Twice()
			producer := NewRabbitMQProducer(ctx, mockReConn, Conf{
				ChannelPoolSize:     2,
				ConfirmationTimeout: time.Millisecond,
				ForcePersistence:    true,
				ForceMandatory:      true,
				ForceInfRejectPub:   true,
			})
			SoMsg("producer is properly created", producer, ShouldNotBeNil)
			SoMsg("with correct confirmation timeout",
				producer.confirmationTimeout, ShouldEqual, time.Millisecond,
			)
			SoMsg("and correct force persistence", producer.forcePersistence, ShouldBeTrue)
			SoMsg("and correct force mandatory", producer.forceMandatory, ShouldBeTrue)
			SoMsg("and correct force infinite reject publish", producer.forceInfRejectPub, ShouldBeTrue)

			closeErr := producer.Close()

			SoMsg("and it properly stops on Close with no error", closeErr, ShouldBeNil)
		})
	})
}

func TestProducer(t *testing.T) {
	Convey("Producer functional tests", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		mockReConn := common.NewMockConnReconnector(cmc)
		mockReChan := common.NewMockChannelReconnector(cmc)
		mockReConn.On("NewReChannel").Return(mockReChan)
		producer := NewRabbitMQProducer(context.Background(), mockReConn, Conf{})
		Reset(func() {
			cmc.Clean()
			SoMsg("close with no error", producer.Close(), ShouldBeNil)
		})
		msg := amqp091.Publishing{
			Body: []byte("test"),
		}

		Convey("on Publish", func() {
			Convey("if everything goes well", func() {
				mockChan := common.NewMockChannel(cmc)
				mockReChan.On("Channel", (*common.QOS)(nil), true).Return(mockChan, error(nil)).Once()
				mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)
				confirmChan := make(chan struct{})
				close(confirmChan)
				mockChan.On(
					"PublishWithDeferredConfirmWithContext", mock.Anything, "exchange", "rk", false, false, msg,
				).Return(mockDeferredConfirm, error(nil)).Once()
				mockDeferredConfirm.On("DeliveryTag").Return(uint64(1))
				mockDeferredConfirm.On("Done").Return((<-chan struct{})(confirmChan)).Once()
				mockDeferredConfirm.On("Acked").Return(true).Once()

				pubErr := producer.Publish("exchange", "rk", msg, nil)
				SoMsg("we get no error", pubErr, ShouldBeNil)
			})
			Convey("if we get a publish error", func(C) {
				expectedPubErr := fmt.Errorf("publish failed")

				mockChan := common.NewMockChannel(cmc)
				mockReChan.On("Channel", (*common.QOS)(nil), true).Return(mockChan, error(nil)).Once()
				mockChan.On(
					"PublishWithDeferredConfirmWithContext", mock.Anything, "exchange", "rk", false, false, msg,
				).Return((common.DeferredConfirmation)(nil), expectedPubErr).Once()

				pubErr := producer.Publish("exchange", "rk", msg, nil)
				SoMsg("we get the expected error", errors.Is(pubErr, expectedPubErr), ShouldBeTrue)
			})
			Convey("if we get a reject publish", func() {
				ackVal := false

				mockChan := common.NewMockChannel(cmc)
				mockReChan.On("Channel", (*common.QOS)(nil), true).Return(mockChan, error(nil)).Once()
				mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)
				mockChan.On(
					"PublishWithDeferredConfirmWithContext", mock.Anything, "exchange", "rk", false, false, msg,
				).Return(mockDeferredConfirm, error(nil)).Once()
				confirmChan := make(chan struct{})
				close(confirmChan)
				mockDeferredConfirm.On("DeliveryTag").Return(uint64(1))
				mockDeferredConfirm.On("Done").Return((<-chan struct{})(confirmChan)).Once()
				mockDeferredConfirm.On("Acked").Return(ackVal).Once()

				pubErr := producer.Publish("exchange", "rk", msg, nil)
				SoMsg(
					"we get the expected error", pubErr.Error(),
					ShouldContainSubstring, "publishing not acknowledged",
				)
			})
		})
		Convey("on PublishASync", func() {
			mockChan := common.NewMockChannel(cmc)
			mockReChan.On("Channel", (*common.QOS)(nil), true).Return(mockChan, error(nil)).Once()
			mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)
			mockChan.On(
				"PublishWithDeferredConfirmWithContext", mock.Anything, "exchange", "rk", false, false, msg,
			).Return(mockDeferredConfirm, error(nil)).Once()
			confirmChan := make(chan struct{})
			close(confirmChan)
			mockDeferredConfirm.On("DeliveryTag").Return(uint64(1))
			mockDeferredConfirm.On("Done").Return((<-chan struct{})(confirmChan)).Once()
			mockDeferredConfirm.On("Acked").Return(true).Once()

			var (
				gotAck     bool
				gotErr     error
				done       = make(chan struct{})
				asyncCheck = make(chan struct{})
			)
			callback := func(ack bool, err error) {
				<-asyncCheck
				gotAck = ack
				gotErr = err
				close(done)
			}

			producer.PublishASync("exchange", "rk", msg, callback, nil)

			close(asyncCheck)
			<-done

			SoMsg("we get expected ack", gotAck, ShouldBeTrue)
			SoMsg("we get no error", gotErr, ShouldBeNil)
		})
		Convey("on PublishASyncWithRetry", func() {
			// all other Publish go through this one, those tests should be more exhaustive
			Convey("with an already closed or closing producer", func() {
				closeErr := producer.Close()
				SoMsg("we get no error on closing", closeErr, ShouldBeNil)
				var (
					ackVal bool
					errVal error
					done   = make(chan struct{})
				)
				producer.PublishASyncWithRetry("", "", amqp091.Publishing{},
					func(ack bool, err error) {
						ackVal = ack
						errVal = err
						close(done)
					}, &backoff.StopBackOff{}, nil, nil)

				<-done
				SoMsg("and the publish gets unacked", ackVal, ShouldBeFalse)
				SoMsg("and the error is EOF", errors.Is(errVal, io.EOF), ShouldBeTrue)
				SoMsg("which is not retriable", amqpwerror.IsErrRetriable(errVal), ShouldBeFalse)
			})
			Convey("with an always failing publish until end of retry", func() {
				mockChan := common.NewMockChannel(cmc)
				mockReChan.On("Channel", (*common.QOS)(nil), true).Return(mockChan, error(nil)).Once()
				pubErr := fmt.Errorf("test publish error")
				// ensure we publish 3 times (base + 2 retries)
				mockChan.On(
					"PublishWithDeferredConfirmWithContext", mock.Anything, "exchange", "rk", false, false, msg,
				).Return(common.DeferredConfirmation(nil), pubErr).Times(3)

				retryPolicy := backoff.WithMaxRetries(backoff.NewConstantBackOff(0), 2)
				var (
					ackVal bool
					errVal error
					done   = make(chan struct{})
				)
				producer.PublishASyncWithRetry("exchange", "rk", msg,
					func(ack bool, err error) {
						ackVal = ack
						errVal = err
						close(done)
					}, retryPolicy, nil, nil)

				<-done
				SoMsg("then the publish gets unacked", ackVal, ShouldBeFalse)
				SoMsg("and the error is the publish error", errors.Is(errVal, pubErr), ShouldBeTrue)
				SoMsg("and the given number or retry is good",
					errVal.Error(), ShouldContainSubstring, "after 2 retries",
				)
				SoMsg("which is retriable (always with publish)", amqpwerror.IsErrRetriable(errVal), ShouldBeTrue)
			})
			Convey("with the persistent & mandatory options", func() {
				mockChan := common.NewMockChannel(cmc)
				mockReChan.On("Channel", (*common.QOS)(nil), true).Return(mockChan, error(nil)).Once()
				mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)

				expectedPersistentMsg := amqp091.Publishing{
					DeliveryMode: amqp091.Persistent,
					Body:         []byte("test"),
				}

				mockChan.On(
					"PublishWithDeferredConfirmWithContext",
					mock.Anything, "exchange", "rk", true, false, expectedPersistentMsg,
				).Return(mockDeferredConfirm, (error)(nil)).Once()

				confirmChan := make(chan struct{})
				close(confirmChan)
				mockDeferredConfirm.On("DeliveryTag").Return(uint64(1))
				mockDeferredConfirm.On("Done").Return((<-chan struct{})(confirmChan)).Once()
				mockDeferredConfirm.On("Acked").Return(true).Once()

				producer.forcePersistence = true

				var (
					gotAck     bool
					gotErr     error
					done       = make(chan struct{})
					asyncCheck = make(chan struct{})
				)
				callback := func(ack bool, err error) {
					<-asyncCheck
					gotAck = ack
					gotErr = err
					close(done)
				}

				producer.PublishASyncWithRetry(
					"exchange", "rk", msg,
					callback, &backoff.StopBackOff{}, nil,
					&ProduceOptions{
						Mandatory: true,
					},
				)

				waitChan := make(chan struct{})
				go func() {
					producer.Wait()
					close(waitChan)
				}()
				var waiting bool
				select {
				case <-waitChan:
				default:
					waiting = true
				}
				SoMsg("Wait() properly blocks when a message is ongoing", waiting, ShouldBeTrue)

				close(asyncCheck)
				<-done
				<-waitChan

				SoMsg("we get expected ack", gotAck, ShouldBeTrue)
				SoMsg("we get no error", gotErr, ShouldBeNil)
			})
			Convey("with infiniteRetry on reject-publish", func() {
				previousIRPB := smartmessage.InfiniteRejectPublishBackoff
				smartmessage.InfiniteRejectPublishBackoff = 0
				defer func() { smartmessage.InfiniteRejectPublishBackoff = previousIRPB }()
				producer.forceInfRejectPub = true

				mockChan := common.NewMockChannel(cmc)
				mockReChan.On("Channel", (*common.QOS)(nil), true).Return(mockChan, error(nil)).Once()
				mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)

				var (
					gotAck     bool
					gotErr     error
					done       = make(chan struct{})
					asyncCheck = make(chan struct{})
				)
				callback := func(ack bool, err error) {
					<-asyncCheck
					gotAck = ack
					gotErr = err
					close(done)
				}

				Convey("with a statusChan (infinite retry ok)", func() {
					statusChan := make(chan error, 6)
					mockChan.On(
						"PublishWithDeferredConfirmWithContext",
						mock.Anything, "exchange", "rk", false, false, msg,
					).Return(mockDeferredConfirm, error(nil)).Times(6)
					confirmChan := make(chan struct{})
					close(confirmChan)
					mockDeferredConfirm.On("DeliveryTag").Return(uint64(1))
					mockDeferredConfirm.On("Done").Return((<-chan struct{})(confirmChan)).Times(6)
					mockDeferredConfirm.On("Acked").Return(false).Times(5)
					mockDeferredConfirm.On("Acked").Return(true).Once()

					// stopbackoff, retry will be thanks to infinite retry system
					producer.PublishASyncWithRetry(
						"exchange", "rk", msg,
						callback, &backoff.StopBackOff{}, statusChan,
						nil,
					)

					close(asyncCheck)
					<-done
					close(statusChan)

					errCount := 0
					for err := range statusChan {
						errCount++
						SoMsg("statusErr retry is reject publish", common.IsErrPublishRejected(err), ShouldBeTrue)
					}
					SoMsg("and we got the expected number of errors in statusChan", errCount, ShouldEqual, 5)

					SoMsg("we get expected ack", gotAck, ShouldBeTrue)
					SoMsg("we get no error", gotErr, ShouldBeNil)
				})

				Convey("with no statusChan (infinite retry ignored)", func() {
					mockChan.On(
						"PublishWithDeferredConfirmWithContext",
						mock.Anything, "exchange", "rk", false, false, msg,
					).Return(mockDeferredConfirm, error(nil)).Once()
					confirmChan := make(chan struct{})
					close(confirmChan)
					mockDeferredConfirm.On("DeliveryTag").Return(uint64(1))
					mockDeferredConfirm.On("Done").Return((<-chan struct{})(confirmChan)).Once()
					mockDeferredConfirm.On("Acked").Return(false).Once()

					// stopbackoff, retry will be thanks to infinite retry system
					producer.PublishASyncWithRetry(
						"exchange", "rk", msg,
						callback, &backoff.StopBackOff{}, nil,
						nil,
					)

					close(asyncCheck)
					<-done

					SoMsg("we get expected unack", gotAck, ShouldBeFalse)
					SoMsg("we get no error", gotErr, ShouldBeNil)
				})
			})
		})
	})
}
