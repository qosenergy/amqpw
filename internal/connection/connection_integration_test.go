//go:build integration

package connection

import (
	"fmt"
	"os"
	"testing"

	"github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/require"

	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/connection/channel"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

func TestReconnector_Connection(t *testing.T) {
	assertion := require.New(t)

	uri, ok := os.LookupEnv(test.AmqpURIKey)
	assertion.True(ok, fmt.Sprintf("%q env var missing", test.AmqpURIKey))

	rco := NewReconnector(uri, "test NewReconnector", nil)

	conn, err := rco.Connection()
	assertion.NoError(err, "new connection")
	assertion.NotNil(rco.(*Reconnector).currConn, "connection up")
	assertion.Equal(conn, rco.(*Reconnector).currConn, "connection stored")

	err = rco.Close()
	assertion.NoError(err, "close connection")
	assertion.Nil(rco.(*Reconnector).currConn, "connection down")
	assertion.Nil(rco.(*Reconnector).currConn, "connection released")
}

func TestReconnector_Channel(t *testing.T) {
	assertion := require.New(t)

	uri, ok := os.LookupEnv(test.AmqpURIKey)
	assertion.True(ok, fmt.Sprintf("%q env var missing", test.AmqpURIKey))

	reconn := NewReconnector(uri, "test Channel reconnector", nil)

	rch := channel.NewReconnector(reconn)

	ch, err := rch.Channel(&common.QOS{}, false)
	assertion.NoError(err, "new channel")

	closeChan := make(chan *amqp091.Error)
	ch.NotifyClose(closeChan)

	err = rch.Close()
	assertion.NoError(err, "close channel")

	_, ok = <-closeChan
	assertion.False(ok, "notification chan closed")
}
