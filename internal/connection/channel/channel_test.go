package channel

import (
	"context"
	"fmt"
	"io"
	"testing"
	"time"

	"github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/qosenergy/amqpw/internal/common"
)

func TestNewReconnector(t *testing.T) {
	t.Run("simple test NewReconnector", func(t *testing.T) {
		mockReConn := common.NewMockConnReconnector(t)

		reconnector := NewReconnector(mockReConn)

		castReconnector, ok := reconnector.(*Reconnector)
		if assert.True(t, ok, "result is a (*Reconnector)") {
			assert.Equal(t, castReconnector.reconn, mockReConn, "connection reConnector OK")
		}
	})
}

func TestClose(t *testing.T) {
	type test struct {
		newReChan   func(*testing.T) *Reconnector
		wait        bool
		expectedErr string
	}

	tests := map[string]test{
		"already closed": {
			newReChan: func(*testing.T) *Reconnector {
				return &Reconnector{
					isClosed: true,
				}
			},
			expectedErr: "",
		},
		"currently not connected (not init or disconnected)": {
			newReChan: func(*testing.T) *Reconnector {
				return &Reconnector{
					currChan: nil,
				}
			},
			expectedErr: "",
		},
		"close OK": {
			newReChan: func(t *testing.T) *Reconnector {
				mChan := common.NewMockChannel(t)
				mChan.On("Close").Return(nil).Once()
				return &Reconnector{
					currChan: mChan,
				}
			},
			expectedErr: "",
		},
		"close KO": {
			newReChan: func(t *testing.T) *Reconnector {
				mChan := common.NewMockChannel(t)
				mChan.On("Close").Return(fmt.Errorf("close fail")).Once()
				return &Reconnector{
					currChan: mChan,
				}
			},
			expectedErr: "close fail",
		},
		"close wait OK": {
			newReChan: func(t *testing.T) *Reconnector {
				mChan := common.NewMockChannel(t)
				mChan.On("Close").Return(nil).Once()
				reco := &Reconnector{
					currChan: mChan,
				}
				reco.wg.Add(1)
				return reco
			},
			expectedErr: "",
			wait:        true,
		},
	}

	for label, tt := range tests {
		t.Run(label, func(t *testing.T) {
			reconn := tt.newReChan(t)

			ctx, cancel := context.WithTimeoutCause(context.Background(), 2*time.Second, fmt.Errorf("timeout"))
			var closeErr error
			go func() {
				closeErr = reconn.Close()
				cancel()
			}()
			if tt.wait {
				time.Sleep(10 * time.Millisecond)
				select {
				case <-ctx.Done():
					assert.Fail(t, "close finish while we expected it to wait")
				default:
				}
				reconn.wg.Done()
			}
			<-ctx.Done()
			if assert.Equal(t, context.Cause(ctx), context.Canceled, "close returned") {
				if tt.expectedErr == "" {
					assert.NoError(t, closeErr, "no error expected")
				} else if assert.Error(t, closeErr, "error expected") {
					assert.Contains(t, closeErr.Error(), tt.expectedErr, "error content OK")
				}
			}
		})
	}
}

func TestChannel(t *testing.T) {
	type test struct {
		newReChan func(t *testing.T) (
			*Reconnector, *common.MockChannel, chan *amqp091.Error,
		)
		qos         *common.QOS
		confirm     bool
		expectedErr string
	}

	tests := map[string]test{
		"new/re conn OK": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockConn := common.NewMockConnection(t)
				mockChan := common.NewMockChannel(t)
				notifClose := make(chan *amqp091.Error)
				mockChan.
					On("NotifyClose", mock.Anything).
					Return(notifClose).
					Once()
				mockConn.
					On("Channel").
					Return(mockChan, nil).
					Once()
				mockReConn.
					On("Connection").
					Return(mockConn, nil).
					Once()
				mockReConn.
					On("SendErr", mock.Anything).
					Once()

				return &Reconnector{
					currChan: nil,
					reconn:   mockReConn,
				}, mockChan, notifClose
			},
		},
		"new/re conn with QOS OK": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockConn := common.NewMockConnection(t)
				mockChan := common.NewMockChannel(t)
				notifClose := make(chan *amqp091.Error)
				mockChan.
					On("Qos", 100, 0, false).
					Return(nil).
					Once()
				mockChan.
					On("NotifyClose", mock.Anything).
					Return(notifClose).
					Once()
				mockConn.
					On("Channel").
					Return(mockChan, nil).
					Once()
				mockReConn.
					On("Connection").
					Return(mockConn, nil).
					Once()
				mockReConn.
					On("SendErr", mock.Anything).
					Once()

				return &Reconnector{
					currChan: nil,
					reconn:   mockReConn,
				}, mockChan, notifClose
			},
			qos: &common.QOS{
				PrefetchCount: 100,
			},
		},
		"new/re conn with confirm OK": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockConn := common.NewMockConnection(t)
				mockChan := common.NewMockChannel(t)
				notifClose := make(chan *amqp091.Error)
				mockChan.
					On("Confirm", false).
					Return(nil).
					Once()
				mockChan.
					On("NotifyClose", mock.Anything).
					Return(notifClose).
					Once()
				mockConn.
					On("Channel").
					Return(mockChan, nil).
					Once()
				mockReConn.
					On("Connection").
					Return(mockConn, nil).
					Once()
				mockReConn.
					On("SendErr", mock.Anything).
					Once()

				return &Reconnector{
					currChan: nil,
					reconn:   mockReConn,
				}, mockChan, notifClose
			},
			confirm: true,
		},
		"conn closed => io.EOF": {
			newReChan: func(*testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				return &Reconnector{
					isClosed: true,
				}, nil, nil
			},
			expectedErr: "EOF",
		},
		"closed => io.EOF": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockReConn.
					On("Connection").
					Return(nil, io.EOF).
					Once()

				return &Reconnector{
					reconn: mockReConn,
				}, nil, nil
			},
			expectedErr: "EOF",
		},
		"already connected": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockChan := common.NewMockChannel(t)

				return &Reconnector{
					currChan: mockChan,
				}, mockChan, nil
			},
		},
		"new/re connection error": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockReConn.
					On("Connection").
					Return(nil, fmt.Errorf("connection error"))

				return &Reconnector{
					currChan: nil,
					reconn:   mockReConn,
				}, nil, nil
			},
			expectedErr: "connection error",
		},
		"new/re channel error": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockConn := common.NewMockConnection(t)

				mockConn.
					On("Channel").
					Return(nil, fmt.Errorf("channel error"))
				mockReConn.
					On("Connection").
					Return(mockConn, nil)

				return &Reconnector{
					currChan: nil,
					reconn:   mockReConn,
				}, nil, nil
			},
			expectedErr: "channel error",
		},
		"new/re confirm error": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockConn := common.NewMockConnection(t)
				mockChan := common.NewMockChannel(t)
				mockChan.
					On("Confirm", false).
					Return(fmt.Errorf("confirm error")).
					Once()
				mockConn.
					On("Channel").
					Return(mockChan, nil).
					Once()
				mockReConn.
					On("Connection").
					Return(mockConn, nil).
					Once()

				return &Reconnector{
					currChan: nil,
					reconn:   mockReConn,
				}, mockChan, nil
			},
			confirm:     true,
			expectedErr: "confirm error",
		},
		"new/re qos error": {
			newReChan: func(t *testing.T) (
				*Reconnector,
				*common.MockChannel,
				chan *amqp091.Error,
			) {
				mockReConn := common.NewMockConnReconnector(t)
				mockConn := common.NewMockConnection(t)
				mockChan := common.NewMockChannel(t)
				mockChan.
					On("Qos", 100, 0, false).
					Return(fmt.Errorf("qos error")).
					Once()
				mockConn.
					On("Channel").
					Return(mockChan, nil).
					Once()
				mockReConn.
					On("Connection").
					Return(mockConn, nil).
					Once()

				return &Reconnector{
					currChan: nil,
					reconn:   mockReConn,
				}, mockChan, nil
			},
			qos: &common.QOS{
				PrefetchCount: 100,
			},
			expectedErr: "qos error",
		},
	}

	for label, tt := range tests {
		t.Run(label, func(t *testing.T) {
			rechan, expectedMockChan, notifCloseChan := tt.newReChan(t)

			resConn, err := rechan.Channel(tt.qos, tt.confirm)
			if tt.expectedErr != "" {
				if assert.Error(t, err, "expect error") {
					assert.Contains(t, err.Error(), tt.expectedErr)
				}
				assert.Nil(t, rechan.currChan, "currChan not set on error")
				return
			}
			if resMockChan, ok := resConn.(*common.MockChannel); assert.True(t, ok, "return channel type OK") {
				assert.Equal(t, resMockChan, expectedMockChan, "result channel OK")
			}
			// test closing
			if notifCloseChan != nil {
				notifCloseChan <- amqp091.ErrClosed
				rechan.wg.Wait()
				assert.Nil(t, rechan.currChan, "channel nil after notifClose")
			}
		})
	}
}
