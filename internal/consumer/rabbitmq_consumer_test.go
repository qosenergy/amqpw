package consumer

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/qosenergy/amqpw/internal/common"
)

func TestConsume(t *testing.T) {
	type test struct {
		newConsumer         func(t *testing.T) (*RabbitMQConsumer, context.CancelFunc)
		expectedMsgs        int
		expectedConsumerErr string
		expectSelfClose     bool
		withCancel          bool // default is with context close
	}

	queueName := "dummy"
	tag1Name := "tag1"

	tests := map[string]test{
		"All OK": {
			newConsumer: func(t *testing.T) (*RabbitMQConsumer, context.CancelFunc) {
				ctx, cancel := context.WithCancel(context.Background())

				reChan := common.NewMockChannelReconnector(t)
				// 3 messages before disconnect
				chan1 := common.NewMockChannel(t)
				chan1out := make(chan amqp091.Delivery, 3)
				var chan1outRet <-chan amqp091.Delivery = chan1out
				for i := 0; i < 3; i++ {
					chan1out <- amqp091.Delivery{Body: []byte(fmt.Sprintf("%d", i))}
				}
				// simulate disconnect
				close(chan1out)
				chan1.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(chan1outRet, nil).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan1, nil).
					Once()
				// then 2 messages
				chan2 := common.NewMockChannel(t)
				chan2out := make(chan amqp091.Delivery, 2)
				var chan2outRet <-chan amqp091.Delivery = chan2out
				for i := 0; i < 2; i++ {
					chan2out <- amqp091.Delivery{Body: []byte(fmt.Sprintf("%d", i))}
				}
				chan2.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(chan2outRet, nil).
					Once()
				chan2.
					On("Cancel", tag1Name, false).
					Run(
						func(_ mock.Arguments) { close(chan2out) },
					).
					Return(nil).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan2, nil).
					Once()

				return &RabbitMQConsumer{
					ctx:        ctx,
					reChannels: []common.ChannelReconnector{reChan},
					backoffBuilder: func() backoff.BackOff {
						return &backoff.StopBackOff{}
					},
					activeConsumers: make(map[string]common.Channel),
				}, cancel
			},
			expectedMsgs: 5,
		},
		"with retry OK": {
			newConsumer: func(t *testing.T) (*RabbitMQConsumer, context.CancelFunc) {
				ctx, cancel := context.WithCancel(context.Background())

				reChan := common.NewMockChannelReconnector(t)
				// 3 messages before disconnect
				chan1 := common.NewMockChannel(t)
				chan1out := make(chan amqp091.Delivery, 3)
				var chan1outRet <-chan amqp091.Delivery = chan1out
				for i := 0; i < 3; i++ {
					chan1out <- amqp091.Delivery{Body: []byte(fmt.Sprintf("%d", i))}
				}
				// simulate disconnect
				close(chan1out)
				chan1.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(chan1outRet, nil).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan1, nil).
					Once()
				// then consume fail
				chan2 := common.NewMockChannel(t)
				chan2.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(nil, fmt.Errorf("some error")).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan2, nil).
					Once()
				reChan.
					On("SendErr", mock.Anything).
					Once()
				// then 2 messages
				chan3 := common.NewMockChannel(t)
				chan3out := make(chan amqp091.Delivery, 2)
				var chan3outRet <-chan amqp091.Delivery = chan3out
				for i := 0; i < 2; i++ {
					chan3out <- amqp091.Delivery{Body: []byte(fmt.Sprintf("%d", i))}
				}
				chan3.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(chan3outRet, nil).
					Once()
				chan3.
					On("Cancel", tag1Name, false).
					Run(
						func(_ mock.Arguments) { close(chan3out) },
					).
					Return(nil)
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan3, nil).
					Once()

				return &RabbitMQConsumer{
					ctx:        ctx,
					reChannels: []common.ChannelReconnector{reChan},
					backoffBuilder: func() backoff.BackOff {
						return backoff.WithMaxRetries(&backoff.ZeroBackOff{}, 1)
					},
					activeConsumers: make(map[string]common.Channel),
				}, cancel
			},
			expectedMsgs: 5,
		},
		"with Cancel Ok": {
			newConsumer: func(t *testing.T) (*RabbitMQConsumer, context.CancelFunc) {
				ctx, cancel := context.WithCancel(context.Background())

				reChan := common.NewMockChannelReconnector(t)
				// 3 messages before disconnect
				chan1 := common.NewMockChannel(t)
				chan1out := make(chan amqp091.Delivery, 3)
				var chan1outRet <-chan amqp091.Delivery = chan1out
				for i := 0; i < 3; i++ {
					chan1out <- amqp091.Delivery{Body: []byte(fmt.Sprintf("%d", i))}
				}
				chan1.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(chan1outRet, nil).
					Once()
				chan1.
					On("Cancel", tag1Name, false).
					Run(
						func(_ mock.Arguments) { close(chan1out) },
					).
					Return(nil).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan1, nil).
					Once()

				return &RabbitMQConsumer{
					ctx:        ctx,
					reChannels: []common.ChannelReconnector{reChan},
					backoffBuilder: func() backoff.BackOff {
						return &backoff.StopBackOff{}
					},
					activeConsumers: make(map[string]common.Channel),
				}, cancel
			},
			expectedMsgs: 3,
			withCancel:   true,
		},
		"with retry Stop": {
			newConsumer: func(t *testing.T) (*RabbitMQConsumer, context.CancelFunc) {
				ctx, cancel := context.WithCancel(context.Background())

				reChan := common.NewMockChannelReconnector(t)
				// 3 messages before disconnect
				chan1 := common.NewMockChannel(t)
				chan1out := make(chan amqp091.Delivery, 3)
				var chan1outRet <-chan amqp091.Delivery = chan1out
				for i := 0; i < 3; i++ {
					chan1out <- amqp091.Delivery{Body: []byte(fmt.Sprintf("%d", i))}
				}
				// simulate disconnect
				close(chan1out)
				chan1.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(chan1outRet, nil).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan1, nil).
					Once()
				// then consume fail
				chan2 := common.NewMockChannel(t)
				chan2.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(nil, fmt.Errorf("some error")).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan2, nil).
					Once()
				reChan.
					On("SendErr", mock.Anything).
					Once()

				return &RabbitMQConsumer{
					ctx:        ctx,
					reChannels: []common.ChannelReconnector{reChan},
					backoffBuilder: func() backoff.BackOff {
						return &backoff.StopBackOff{}
					},
					activeConsumers: make(map[string]common.Channel),
				}, cancel
			},
			expectedMsgs:    3,
			expectSelfClose: true,
		},
		"init consume err KO": {
			newConsumer: func(t *testing.T) (*RabbitMQConsumer, context.CancelFunc) {
				ctx, cancel := context.WithCancel(context.Background())

				reChan := common.NewMockChannelReconnector(t)
				chan1 := common.NewMockChannel(t)
				chan1.
					On("Consume", queueName, tag1Name, false, false, false, false, amqp091.Table{}).
					Return(nil, fmt.Errorf("first consume error")).
					Once()
				reChan.
					On("Channel", (*common.QOS)(nil), false).
					Return(chan1, nil).
					Once()

				return &RabbitMQConsumer{
					ctx:        ctx,
					reChannels: []common.ChannelReconnector{reChan},
					backoffBuilder: func() backoff.BackOff {
						return &backoff.StopBackOff{}
					},
					activeConsumers: make(map[string]common.Channel),
				}, cancel
			},
			expectedConsumerErr: "first consume error",
		},
		"ctx Done => EOF KO": {
			newConsumer: func(*testing.T) (*RabbitMQConsumer, context.CancelFunc) {
				ctx, cancel := context.WithCancel(context.Background())
				// cancel immediately to test closed consume
				cancel()

				return &RabbitMQConsumer{
					ctx:        ctx,
					reChannels: nil,
					backoffBuilder: func() backoff.BackOff {
						return &backoff.StopBackOff{}
					},
					activeConsumers: make(map[string]common.Channel),
				}, cancel
			},
			expectedConsumerErr: "EOF",
		},
	}

	for label, tt := range tests {
		t.Run(label, func(t *testing.T) {
			assertion := require.New(t)
			consumer, cancel := tt.newConsumer(t)
			outChan, err := consumer.Consume("dummy", "tag1", nil)
			if tt.expectedConsumerErr != "" {
				assertion.Error(err, "got expected error")
				assertion.Contains(err.Error(), tt.expectedConsumerErr, "consume error has expected msg")
				return
			}
			assertion.NoError(err, "consumer no error")
			testCtx, timeoutCancel := context.WithTimeoutCause(context.Background(), time.Second, fmt.Errorf("timeout"))
			for i := 0; i < tt.expectedMsgs; i++ {
				select {
				case _, ok := <-outChan:
					assertion.Truef(ok, "received msg %d OK", i)
				case <-testCtx.Done():
					assertion.Fail("timeout waiting expectedMsgs")
				}
			}

			if !tt.expectSelfClose {
				if tt.withCancel {
					_ = consumer.Cancel(tag1Name)
				} else {
					// close consumer ctx
					cancel()
				}
			}

			select {
			case _, ok := <-outChan:
				assertion.False(ok, "outChan properly closed on context close")
			case <-testCtx.Done():
				assertion.Fail("timeout waiting consumer close from ctx")
			}
			timeoutCancel()
		})
	}
}
