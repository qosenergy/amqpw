// Package common for common tools intern to amqpw v3
package common

//go:generate mockery

import (
	"context"

	"github.com/rabbitmq/amqp091-go"
)

// ChannelReconnector is the interface for all amqp channel reconnection system (Pool & Reconnector)
type ChannelReconnector interface {
	// Channel returns a channel set up with given QOS (if it is a new one)
	// and with confirm mode if bool is true
	Channel(*QOS, bool) (Channel, error)
	Close() error
	SendErr(error)
}

// ConnReconnector is the interface of connection.Reconnector
type ConnReconnector interface {
	Connection() (Connection, error)
	Close() error
	SendErr(error)
	NewReChannel() ChannelReconnector
}

// Connection abstracts amqp091.Connection to allow easy mocking
type Connection interface {
	/*
		NotifyClose registers a listener for close events either initiated
		by an error accompanying a connection.close method or by a normal shutdown.

		The chan provided will be closed when the Connection is closed and on a graceful close, no error will be sent.

		In case of a non graceful close the error will be notified synchronously by the library so that it will
		be necessary to consume the Channel from the caller in order to avoid deadlocks

		To reconnect after a transport or protocol error, register a listener here and re-run your setup process.
	*/
	NotifyClose(chan *amqp091.Error) chan *amqp091.Error
	/*
		Close requests and waits for the response to close the AMQP connection.

		It's advisable to use this message when publishing to ensure all kernel buffers
		have been flushed on the server and client before exiting.

		An error indicates that server may not have received this request to close but
		the connection should be treated as closed regardless.

		After returning from this call, all resources associated with this connection,
		including the underlying io, Channels, Notify listeners and Channel consumers
		will also be closed.
	*/
	Close() error
	/*
		Channel opens a unique, concurrent server channel to process the bulk of AMQP messages.
		Any error from methods on this receiver will render the receiver invalid and a new Channel should be opened.
	*/
	Channel() (Channel, error)
}

// Channel abstracts amqp091.Channel to allow easy mocking
type Channel interface {
	/*
		NotifyClose registers a listener for when the server sends a channel or
		connection exception in the form of a Connection.Close or Channel.Close method.
		Connection exceptions will be broadcast to all open channels and all channels
		will be closed, where channel exceptions will only be broadcast to listeners to
		this channel.

		The chan provided will be closed when the Channel is closed and on a
		graceful close, no error will be sent.

		In case of a non graceful close the error will be notified synchronously by the library
		so that it will be necessary to consume the Channel from the caller in order to avoid deadlocks
	*/
	NotifyClose(receiver chan *amqp091.Error) chan *amqp091.Error
	/*
		Cancel stops deliveries to the consumer chan established in Channel.Consume and
		identified by consumer.

		Only use this method to cleanly stop receiving deliveries from the server and
		cleanly shut down the consumer chan identified by this tag.  Using this method
		and waiting for remaining messages to flush from the consumer chan will ensure
		all messages received on the network will be delivered to the receiver of your
		consumer chan.

		Continue consuming from the chan Delivery provided by Channel.Consume until the
		chan closes.

		When noWait is true, do not wait for the server to acknowledge the cancel.
		Only use this when you are certain there are no deliveries in flight that
		require an acknowledgment, otherwise they will arrive and be dropped in the
		client without an ack, and will not be redelivered to other consumers.
	*/
	Cancel(consumer string, noWait bool) error
	/*
		Close initiate a clean channel closure by sending a close message with the error
		code set to '200'.

		It is safe to call this method multiple times.
	*/
	Close() error

	/***************************** CONSUME FUNCS *****************************/

	/*
		Qos controls how many messages or how many bytes the server will try to keep on
		the network for consumers before receiving delivery acks.  The intent of Qos is
		to make sure the network buffers stay full between the server and client.

		With a prefetch count greater than zero, the server will deliver that many
		messages to consumers before acknowledgments are received.  The server ignores
		this option when consumers are started with noAck because no acknowledgments
		are expected or sent.

		With a prefetch size greater than zero, the server will try to keep at least
		that many bytes of deliveries flushed to the network before receiving
		acknowledgments from the consumers.  This option is ignored when consumers are
		started with noAck.

		When global is true, these Qos settings apply to all existing and future
		consumers on all channels on the same connection.  When false, the Channel.Qos
		settings will apply to all existing and future consumers on this channel.

		Please see the RabbitMQ Consumer Prefetch documentation for an explanation of
		how the global flag is implemented in RabbitMQ, as it differs from the
		AMQP 0.9.1 specification in that global Qos settings are limited in scope to
		channels, not connections (https://www.rabbitmq.com/consumer-prefetch.html).

		To get round-robin behavior between consumers consuming from the same queue on
		different connections, set the prefetch count to 1, and the next available
		message on the server will be delivered to the next available consumer.

		If your consumer work time is reasonably consistent and not much greater
		than two times your network round trip time, you will see significant
		throughput improvements starting with a prefetch count of 2 or slightly
		greater as described by benchmarks on RabbitMQ.

		http://www.rabbitmq.com/blog/2012/04/25/rabbitmq-performance-measurements-part-2/
	*/
	Qos(prefetchCount, prefetchSize int, global bool) error
	/*
		Consume immediately starts delivering queued messages.

		Begin receiving on the returned chan Delivery before any other operation on the
		Connection or Channel.

		Continues deliveries to the returned chan Delivery until Channel.Cancel,
		Connection.Close, Channel.Close, or an AMQP exception occurs.  Consumers must
		range over the chan to ensure all deliveries are received.  Unreceived
		deliveries will block all methods on the same connection.

		All deliveries in AMQP must be acknowledged.  It is expected of the consumer to
		call Delivery.Ack after it has successfully processed the delivery.  If the
		consumer is cancelled or the channel or connection is closed any unacknowledged
		deliveries will be requeued at the end of the same queue.

		The consumer is identified by a string that is unique and scoped for all
		consumers on this channel.  If you wish to eventually cancel the consumer, use
		the same non-empty identifier in Channel.Cancel.  An empty string will cause
		the library to generate a unique identity.  The consumer identity will be
		included in every Delivery in the ConsumerTag field

		When autoAck (also known as noAck) is true, the server will acknowledge
		deliveries to this consumer prior to writing the delivery to the network.  When
		autoAck is true, the consumer should not call Delivery.Ack. Automatically
		acknowledging deliveries means that some deliveries may get lost if the
		consumer is unable to process them after the server delivers them.
		See http://www.rabbitmq.com/confirms.html for more details.

		When exclusive is true, the server will ensure that this is the sole consumer
		from this queue. When exclusive is false, the server will fairly distribute
		deliveries across multiple consumers.

		The noLocal flag is not supported by RabbitMQ.

		It's advisable to use separate connections for
		Channel.Publish and Channel.Consume so not to have TCP pushback on publishing
		affect the ability to consume messages, so this parameter is here mostly for
		completeness.

		When noWait is true, do not wait for the server to confirm the request and
		immediately begin deliveries.  If it is not possible to consume, a channel
		exception will be raised and the channel will be closed.

		Optional arguments can be provided that have specific semantics for the queue
		or server.

		Inflight messages, limited by Channel.Qos will be buffered until received from
		the returned chan.

		When the Channel or Connection is closed, all buffered and inflight messages will
		be dropped. RabbitMQ will requeue messages not acknowledged. In other words, dropped
		messages in this way won't be lost.

		When the consumer tag is cancelled, all inflight messages will be delivered until
		the returned chan is closed.
	*/
	Consume(
		queue, consumer string, autoAck, exclusive, noLocal, noWait bool, args amqp091.Table,
	) (<-chan amqp091.Delivery, error)

	/***************************** PUBLISH FUNCS *****************************/

	/*
		NotifyPublish registers a listener for reliable publishing. Receives from this
		chan for every publish after Channel.Confirm will be in order starting with
		DeliveryTag 1.

		There will be one and only one Confirmation Publishing starting with the
		delivery tag of 1 and progressing sequentially until the total number of
		Publishings have been seen by the server.

		Acknowledgments will be received in the order of delivery from the
		NotifyPublish channels even if the server acknowledges them out of order.

		The listener chan will be closed when the Channel is closed.

		The capacity of the chan Confirmation must be at least as large as the
		number of outstanding publishings.  Not having enough buffered chans will
		create a deadlock if you attempt to perform other operations on the Connection
		or Channel while confirms are in-flight.

		It's advisable to wait for all Confirmations to arrive before calling
		Channel.Close() or Connection.Close().

		It is also advisable for the caller to consume from the channel returned till it is closed
		to avoid possible deadlocks
	*/
	NotifyPublish(confirm chan amqp091.Confirmation) chan amqp091.Confirmation
	/*
		Confirm puts this channel into confirm mode so that the client can ensure all
		publishings have successfully been received by the server.  After entering this
		mode, the server will send a basic.ack or basic.nack message with the deliver
		tag set to a 1 based incremental index corresponding to every publishing
		received after the this method returns.

		Add a listener to Channel.NotifyPublish to respond to the Confirmations. If
		Channel.NotifyPublish is not called, the Confirmations will be silently
		ignored.

		The order of acknowledgments is not bound to the order of deliveries.

		Ack and Nack confirmations will arrive at some point in the future.

		Unroutable mandatory or immediate messages are acknowledged immediately after
		any Channel.NotifyReturn listeners have been notified.  Other messages are
		acknowledged when all queues that should have the message routed to them have
		either received acknowledgment of delivery or have enqueued the message,
		persisting the message if necessary.

		When noWait is true, the client will not wait for a response.  A channel
		exception could occur if the server does not support this method.
	*/
	Confirm(noWait bool) error
	/*
		PublishWithContext sends a Publishing from the client to an exchange on the server.

		When you want a single message to be delivered to a single queue, you can
		publish to the default exchange with the routingKey of the queue name.  This is
		because every declared queue gets an implicit route to the default exchange.

		Since publishings are asynchronous, any undeliverable message will get returned
		by the server.  Add a listener with Channel.NotifyReturn to handle any
		undeliverable message when calling publish with either the mandatory or
		immediate parameters as true.

		Publishings can be undeliverable when the mandatory flag is true and no queue is
		bound that matches the routing key, or when the immediate flag is true and no
		consumer on the matched queue is ready to accept the delivery.

		This can return an error when the channel, connection or socket is closed.  The
		error or lack of an error does not indicate whether the server has received this
		publishing.

		It is possible for publishing to not reach the broker if the underlying socket
		is shut down without pending publishing packets being flushed from the kernel
		buffers.  The easy way of making it probable that all publishings reach the
		server is to always call Connection.Close before terminating your publishing
		application.  The way to ensure that all publishings reach the server is to add
		a listener to Channel.NotifyPublish and put the channel in confirm mode with
		Channel.Confirm.  Publishing delivery tags and their corresponding
		confirmations start at 1.  Exit when all publishings are confirmed.

		When Publish does not return an error and the channel is in confirm mode, the
		internal counter for DeliveryTags with the first confirmation starts at 1.
	*/
	PublishWithContext(
		ctx context.Context, exchange, key string, mandatory, immediate bool, msg amqp091.Publishing,
	) error

	/*
		PublishWithDeferredConfirmWithContext behaves identically to PublishWithContext but additionally returns a
		DeferredConfirmation, allowing the caller to wait on the publisher confirmation
		for this message. If the channel has not been put into confirm mode,
		the DeferredConfirmation will be nil.
	*/
	PublishWithDeferredConfirmWithContext(
		ctx context.Context, exchange, routingKey string, mandatory, immediate bool, msg amqp091.Publishing,
	) (DeferredConfirmation, error)

	/*
		ExchangeDeclarePassive attempts to connect to an existing exchange.
		If it does not exist, it will cause RabbitMQ to throw an exception.
		This function can be used to detect the existence of an exchange.
	*/
	ExchangeDeclarePassive(name, kind string, durable, autoDelete, internal, noWait bool, args amqp091.Table) error

	/*
		QueueDeclarePassive attempts to connect to an existing queue.
		If it does not exist, it will cause RabbitMQ to throw an exception.
		This function can be used to detect the existence of a queue.
	*/
	QueueDeclarePassive(
		name string, durable, autoDelete, exclusive, noWait bool, args amqp091.Table,
	) (amqp091.Queue, error)
}

// DeferredConfirmation is an interface to get amqp091.DeferredConfirmation features while allowing tests by mock
type DeferredConfirmation interface {
	DeliveryTag() uint64
	Acked() bool
	Done() <-chan struct{}
}
