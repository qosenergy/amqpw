package common

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotNilWrap(t *testing.T) {
	type test struct {
		msg            string
		err            error
		wraps          []func(error) error
		expectedErr    string
		validationFunc func(error) bool
	}

	tests := map[string]test{
		"nil error": {
			msg: "this is not an error: %w",
			err: nil,
			wraps: []func(error) error{
				func(err error) error {
					return fmt.Errorf("this neither: %w", err)
				},
			},
			expectedErr: "",
		},
		"no wrap error": {
			msg:         "this is an error: %w",
			err:         fmt.Errorf("pomme"),
			expectedErr: "this is an error: pomme",
		},
		"double wrap error": {
			msg:         "this is an error: %w",
			err:         fmt.Errorf("pomme"),
			expectedErr: "2: 1: this is an error: pomme",
			wraps: []func(error) error{
				func(err error) error {
					return fmt.Errorf("1: %w", err)
				},
				func(err error) error {
					return fmt.Errorf("2: %w", err)
				},
			},
		},
		"double wrap typed error": {
			msg:         "this is an error: %w",
			err:         fmt.Errorf("pomme"),
			expectedErr: "wrapped retriable: this is an error: pomme",
			wraps: []func(error) error{
				func(err error) error {
					return fmt.Errorf("wrapped retriable: %w", err)
				},
				SetRetriableErr,
			},
			validationFunc: IsErrRetriable,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			assertions := assert.New(t)
			newErr := NotNilWrap(tt.msg, tt.err, tt.wraps...)
			if tt.expectedErr == "" {
				assertions.NoError(newErr, "no error")
				return
			}
			if assertions.Error(newErr, "expected error") {
				assertions.Contains(newErr.Error(), tt.expectedErr)
				if tt.validationFunc != nil {
					assertions.True(tt.validationFunc(newErr), "validate error")
				}
			}
		})
	}
}

func TestError(t *testing.T) {
	type test struct {
		err               error
		expectedRetriable bool
	}
	tests := map[string]test{
		"nothing": {
			err: fmt.Errorf("boarf"),
		},
		"retriable alone": {
			err:               SetRetriableErr(fmt.Errorf("retriable")),
			expectedRetriable: true,
		},
		"retriable wrapped": {
			err: fmt.Errorf(
				"wrapped: %w",
				SetRetriableErr(fmt.Errorf("both")),
			),
			expectedRetriable: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			assertions := assert.New(t)
			unwrap := tt.err
			for {
				unwrap = errors.Unwrap(unwrap)
				if unwrap == nil {
					break
				}
			}
			assertions.Equal(tt.expectedRetriable, IsErrRetriable(tt.err), "retriable OK")
		})
	}
}
