package common

// QOS is the Quality Of Service used by AMQP to identify how many messages to lock before ack
type QOS struct {
	PrefetchCount int
	PrefetchSize  int
	Global        bool
}
