package common

import (
	"errors"
	"fmt"
)

// NotNilWrap wraps an error only if not nil & warps it with the optionnal wrap funcs.
// msgW must contain "%w" to include given errorf
func NotNilWrap(msgW string, err error, wraps ...func(error) error) error {
	if err == nil {
		return nil
	}
	err = fmt.Errorf(msgW, err)
	for _, wrap := range wraps {
		err = wrap(err)
	}
	return err
}

// SetRetriableErr wraps the error so it's identified as a retriable one
func SetRetriableErr(err error) error {
	return fmt.Errorf("%w", IsRetriable{err})
}

// IsRetriable is a retriable error
type IsRetriable struct {
	error
}

// Unwrap for full compatibility with errors.Is & errors.As
func (retry IsRetriable) Unwrap() error {
	return retry.error
}

// IsErrRetriable returns true if the error is retriable (or wraps a retriable)
func IsErrRetriable(err error) bool {
	retriableErr := &IsRetriable{}
	return errors.As(err, retriableErr)
}

// SetRejectPublishErr wraps the error so it's identified as a reject publish (queue full or no queue)
// It is also set as retriable.
func SetRejectPublishErr(err error) error {
	return SetRetriableErr(fmt.Errorf("%w", IsPublishRejected{err}))
}

// IsPublishRejected is a publish reject error (reject-publish or no queue with mandatory)
type IsPublishRejected struct {
	error
}

// Unwrap for full compatibility with errors.Is & errors.As
func (pubReject IsPublishRejected) Unwrap() error {
	return pubReject.error
}

// IsErrPublishRejected returns true if the error is a reject publish (or wraps one)
// reject-publish error are replaced by nil error if the publish fails because of one,
// so this func does not need to be in a public lib.
func IsErrPublishRejected(err error) bool {
	PublishRejected := &IsPublishRejected{}
	return errors.As(err, PublishRejected)
}
