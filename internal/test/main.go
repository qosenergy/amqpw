// Package test implements integration test specific utilitary functions
package test

import (
	"context"
	"fmt"
	"time"

	"github.com/rabbitmq/amqp091-go"
)

// AmqpURIKey stores the name of the env var to use across tests for local rabbitmq
const AmqpURIKey = "AMQP_URI"

// Publish publishes a message to RMQ (without amqpw)
func Publish(uri string, queue string, body []byte) error {
	conn, ch, err := getChan(uri)
	if err != nil {
		return fmt.Errorf("get channel: %w", err)
	}
	defer func() { _ = conn.Close() }()

	_, err = ch.QueueDeclare(queue, true, false, false, false, amqp091.Table{})
	if err != nil {
		return fmt.Errorf("creating queue: %w", err)
	}

	publishing := amqp091.Publishing{Body: body}

	return ch.PublishWithContext(
		context.Background(),
		"",
		queue,
		false,
		false,
		publishing,
	)
}

// Consume consumes a message from RMQ (without amqpw)
func Consume(uri string, queue string) (amqp091.Delivery, error) {
	conn, ch, err := getChan(uri)
	if err != nil {
		return amqp091.Delivery{}, fmt.Errorf("get channel: %w", err)
	}
	defer func() { _ = conn.Close() }()

	_, err = ch.QueueDeclare(queue, true, false, false, false, amqp091.Table{})
	if err != nil {
		return amqp091.Delivery{}, fmt.Errorf("creating queue: %w", err)
	}

	deliveryChan, consumeErr := ch.ConsumeWithContext(
		context.Background(),
		queue,
		"test",
		true,
		false,
		false,
		false,
		amqp091.Table{},
	)
	if consumeErr != nil {
		return amqp091.Delivery{}, fmt.Errorf("consume: %w", consumeErr)
	}

	timer := time.NewTimer(time.Second)
	select {
	case msg := <-deliveryChan:
		timer.Stop()
		return msg, nil
	case <-timer.C:
		return amqp091.Delivery{}, fmt.Errorf("no msg to consume")
	}
}

// QueueDeclare declares a new queue
func QueueDeclare(uri, queue string) error {
	conn, ch, err := getChan(uri)
	if err != nil {
		return fmt.Errorf("get channel: %w", err)
	}
	defer func() { _ = conn.Close() }()

	_, err = ch.QueueDeclare(queue, true, false, false, false, amqp091.Table{})
	if err != nil {
		return fmt.Errorf("creating queue: %w", err)
	}
	_, err = ch.QueuePurge(queue, false)
	if err != nil {
		return fmt.Errorf("purging queue: %w", err)
	}
	return nil
}

// ExchangeDeclare declares a new exchange
func ExchangeDeclare(uri, exchange, kind string) error {
	conn, ch, err := getChan(uri)
	if err != nil {
		return fmt.Errorf("get channel: %w", err)
	}
	defer func() { _ = conn.Close() }()

	err = ch.ExchangeDeclare(
		exchange, kind,
		false, false, false, false,
		amqp091.Table{},
	)
	if err != nil {
		return fmt.Errorf("creating exchange: %w", err)
	}
	return nil
}

// ExchangeDelete deletes an exchange
func ExchangeDelete(uri, exchange string) error {
	conn, ch, err := getChan(uri)
	if err != nil {
		return fmt.Errorf("get channel: %w", err)
	}
	defer func() { _ = conn.Close() }()

	err = ch.ExchangeDelete(
		exchange,
		false, false,
	)
	if err != nil {
		return fmt.Errorf("deleting exchange: %w", err)
	}
	return nil
}

// QueueDelete deletes an exchange
func QueueDelete(uri, queue string) error {
	conn, ch, err := getChan(uri)
	if err != nil {
		return fmt.Errorf("get channel: %w", err)
	}
	defer func() { _ = conn.Close() }()

	_, err = ch.QueueDelete(
		queue,
		false, false, false,
	)
	if err != nil {
		return fmt.Errorf("deleting queue: %w", err)
	}
	return nil
}

// BindDeclare declares a binding between an exchange and a queue
func BindDeclare(uri, exchange, queue, key string) error {
	conn, ch, err := getChan(uri)
	if err != nil {
		return fmt.Errorf("get channel: %w", err)
	}
	defer func() { _ = conn.Close() }()

	err = ch.QueueBind(queue, key, exchange, false, amqp091.Table{})
	if err != nil {
		return fmt.Errorf("binding queue: %w", err)
	}
	return nil
}

func getChan(uri string) (*amqp091.Connection, *amqp091.Channel, error) {
	conn, err := amqp091.Dial(uri)
	if err != nil {
		return nil, nil, fmt.Errorf("connecting to RabbitMQ: %w", err)
	}

	ch, err := conn.Channel()
	if err != nil {
		_ = conn.Close()
		return nil, nil, fmt.Errorf("creating channel: %w", err)
	}

	return conn, ch, nil
}
