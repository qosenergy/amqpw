package test

import (
	"strings"
	"testing"

	"github.com/smartystreets/goconvey/convey"
)

// ConveyMockCleanup allows simple interaction between mockery's automatic assert expectations
// and Convey
type ConveyMockCleanup struct {
	*testing.T
	c          convey.C
	cleanFuncs *[]func()
}

// NewConveyMockCleanup creates a new convey mock cleanup
// (t is use for default behaviour & test interruption,
// since convey does not support interruption in goroutines)
func NewConveyMockCleanup(t *testing.T, c convey.C) *ConveyMockCleanup {
	return &ConveyMockCleanup{
		T:          t,
		c:          c,
		cleanFuncs: &[]func(){},
	}
}

// Cleanup has a similar behaviour to (*testing.T).Cleanup: it register the given function so it is
// ran in reverse order on cleanup.
// To cleanup, call Clean() in  convey.Reset()
func (cc *ConveyMockCleanup) Cleanup(f func()) {
	*cc.cleanFuncs = append(*cc.cleanFuncs, f)
}

// Errorf logs the associated error and stops the test.
// Since Convey currently handles this badly with goroutine, we use T directly.
func (cc *ConveyMockCleanup) Errorf(format string, args ...any) {
	cc.T.Errorf("\n"+format, args...)
}

// Logf has been updated to only log FAIL from assertion
// it serves to make assert.expectation less verbose, as Convey avoids logging to much
func (cc *ConveyMockCleanup) Logf(format string, args ...any) {
	if strings.Contains(format, "FAIL") {
		cc.T.Logf(format, args...)
	}
}

// Clean should be called in convey.Reset() to run every registered Cleanup at the end of each test
func (cc *ConveyMockCleanup) Clean() {
	for i := len(*cc.cleanFuncs) - 1; i >= 0; i-- {
		(*cc.cleanFuncs)[i]()
	}
}
