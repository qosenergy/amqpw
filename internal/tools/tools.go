// Package tools contains tools to ensure the topology is properly configured before starting processing
package tools

import (
	"fmt"

	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/qosenergy/amqpw/internal/common"
)

// Tools implements punctual check tools for amqp
type Tools struct {
	conn common.ConnReconnector
}

// NewTools creates a Tools from a reconnector
func NewTools(conn common.ConnReconnector) *Tools {
	return &Tools{
		conn: conn,
	}
}

func (t *Tools) getOneTimeChannel() (common.Channel, error) {
	conn, connErr := t.conn.Connection()
	if connErr != nil {
		return nil, fmt.Errorf("get connection: %w", connErr)
	}
	channel, channelErr := conn.Channel()
	if channelErr != nil {
		return nil, fmt.Errorf("creating channel: %w", channelErr)
	}
	return channel, nil
}

// CheckQueue checks if the given queue exists (& returns some of its metadata if it does).
//
// Error is not nil only if some unexpected error occures (it is nil if the queue does not exist)
func (t *Tools) CheckQueue(queueName string) (bool, *amqp091.Queue, error) {
	channel, channelErr := t.getOneTimeChannel()
	if channelErr != nil {
		return false, nil, fmt.Errorf("get on time channel: %w", channelErr)
	}
	// close the channel once test is done
	defer func() { _ = channel.Close() }()

	queue, queueErr := channel.QueueDeclarePassive(
		queueName,
		false, false, false, false, amqp091.Table{},
	)
	if queueErr != nil {
		if amqpErr, ok := queueErr.(*amqp091.Error); ok {
			if amqpErr.Code == 404 {
				return false, nil, nil
			}
		}
		return false, nil, fmt.Errorf("getting queue %s: %w", queueName, queueErr)
	}
	return true, &queue, nil
}

// CheckExchange checks if the given exchange exists.
//
// Error is not nil only if some unexpected error occures (it is nil if the exchange does not exist)
func (t *Tools) CheckExchange(exchangeName string) (bool, error) {
	channel, channelErr := t.getOneTimeChannel()
	if channelErr != nil {
		return false, fmt.Errorf("get on time channel: %w", channelErr)
	}
	// close the channel once test is done
	defer func() { _ = channel.Close() }()

	exchangeErr := channel.ExchangeDeclarePassive(
		exchangeName,
		amqp091.ExchangeFanout, // this parameter is ignored in ExchangeDeclarePassive
		false, false, false, false, amqp091.Table{},
	)
	if exchangeErr != nil {
		if amqpErr, ok := exchangeErr.(*amqp091.Error); ok {
			if amqpErr.Code == 404 {
				return false, nil
			}
		}
		return false, fmt.Errorf("getting exchange %s: %w", exchangeName, exchangeErr)
	}
	return true, nil
}
